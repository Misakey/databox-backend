"databoxes":
  post:
    summary: create a databox
    description: "Create a databox which is related to a user and a data producing application."
    tags:
    - Databoxes
    requestBody:
      description: a databox information
      content:
        application/json:
          schema:
            $ref: "#/components/schemas/Databox"
    responses:
      '201':
        description: status created
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/Databox"
      '401':
        $ref: "common_responses.yaml#/components/responses/401Unauthorized"
      '403':
        $ref: "common_responses.yaml#/components/responses/403Forbidden"
      '404':
        $ref: "common_responses.yaml#/components/responses/404NotFound"
      '409':
        $ref: "common_responses.yaml#/components/responses/409Conflict"

  get:
    summary: list databoxes
    description: "List databoxes according to filters and accesses"
    tags:
    - Databoxes
    parameters:
    - in: query
      name: producer_id
      schema:
        type: string
        format: uuid
        example: "00000000-1111-2222-3333-444444444444"
      description: id of the producer
    - in: query
      name: with_users
      schema:
        type: boolean
        example: true
      description: add user's information to databoxes
    - in: query
      name: limit
      schema:
        type: number
        example: 30
      description: maximum number of items that must be returned
    - in: query
      name: offset
      schema:
        type: number
        example: 30
      description: position of the first item returned by the server in database
    responses:
      '200':
        description: success
        content:
          application/json:
            schema:
              type: array
              items:
                $ref: "#/components/schemas/Databox"
      '400':
        $ref: 'common_responses.yaml#/components/responses/400BadRequest'
      '401':
        $ref: "common_responses.yaml#/components/responses/401Unauthorized"

"get-databox":
  get:
    summary: get a databox with its uuid
    description: "Get a databox based on its uuid."
    tags:
    - Databoxes
    parameters:
    - in: path
      name: id
      schema:
        type: string
        format: uuid
      description: uuid of the databox
      required: true
    responses:
      '200':
        description: success
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/Databox"
      '400':
        $ref: 'common_responses.yaml#/components/responses/400BadRequest'
      '401':
        $ref: "common_responses.yaml#/components/responses/401Unauthorized"
      '403':
        $ref: "common_responses.yaml#/components/responses/403Forbidden"
      '404':
        $ref: "common_responses.yaml#/components/responses/404NotFound"

"databox-access-requests-by-token":
  get:
    summary: get a databox access request using its token
    description: "Get a databox access request information using its token."
    tags:
    - Databoxes
    parameters:
    - in: path
      name: token
      schema:
        type: string
      description: token of the access request
      required: true
    responses:
      '200':
        description: success
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/DataboxAccessRequest"
      '404':
        $ref: "common_responses.yaml#/components/responses/404NotFound"

"databox-access-requests":
  get:
    summary: get a databox access request
    description: "Get a databox access request which is related to a databox and contains a token that can be use to send a confirmation code to the data producing application dpo.
     The access request is auto-generated the first time."
    tags:
    - Databoxes
    parameters:
    - in: path
      name: id
      schema:
        type: string
      description: databox id to link to the created access request
      required: true
    responses:
      '201':
        description: status created
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/DataboxAccessRequest"
      '401':
        $ref: "common_responses.yaml#/components/responses/401Unauthorized"
      '403':
        $ref: "common_responses.yaml#/components/responses/403Forbidden"
      '404':
        $ref: "common_responses.yaml#/components/responses/404NotFound"

"blobs":
  put:
    summary: upload a blob
    description: "Upload a blob to a databox and return the newly assigned id of the blob"
    tags:
    - Blobs
    requestBody:
      required: true
      content:
        multipart/form-data:
          schema:
            type: object
            title: Upload Blob
            description: the encrypted data - not size limit today
            properties:
              blob:
                type: object
                description: blob form file
              transaction_id:
                type: string
                description: a random value that the client generates on first sending and will re-use to mitigate multi upload in case of network failures - must be unique
              data_type:
                type: string
                description: type of the data - should be "all".
              databox_id:
                type: string
                description: the id of the databox
              encryption[algorithm]:
                type: string
                description: must be "com.misakey.nacl-box"
              encryption[nonce]:
                type: string
                description: (base64-encoded bytes) the nonce (non-secret source of randomness for encryption) that was used during encryption
              encryption[ephemeral_producer_pub_key]:
                type: string
                description: (base64-encoded bytes) the ephemeral public key created by the producer to encrypt the data
              encryption[owner_pub_key]:
                type: string
                description: (base64-encoded bytes) the public key of the data owner who owns this blob (to keep track of what was his public key at the time this blob was encrypted, in case it changes)
              file_extension:
                description: The file extension - should be format as .*
                type: string
            required:
              - blob
              - transaction_id
              - data_type
              - databox_id
              - encryption[algorithm]
              - encryption[nonce]
              - encryption[ephemeral_producer_pub_key]
              - encryption[owner_pub_key]
              - file_extension
    responses:
      '200':
        description: success
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/DataboxBlobMetadatum"
      '400':
        $ref: 'common_responses.yaml#/components/responses/400BadRequest'
      '403':
        $ref: 'common_responses.yaml#/components/responses/403Forbidden'
      '404':
        $ref: "common_responses.yaml#/components/responses/404NotFound"

  get:
    summary: list blobs
    description: "List blobs metadata according to filters"
    tags:
    - Blobs
    parameters:
    - in: query
      name: limit
      schema:
        type: number
      description: maximum number of items that must be returned
    - in: query
      name: offset
      schema:
        type: number
      description: position of the first item returned by the server in database
    - in: query
      name: created_before
      schema:
        type: string
        format: date-time
      description: maximum creation date of items to return
    - in: query
      name: created_after
      schema:
        type: string
        format: date-time
      description: minimum creation date of items to return
    - in: query
      name: databox_ids
      schema:
        type: string
      description: comma-separated list of uuids, a filter to only include blobs in these databoxes
    - in: query
      name: data_types
      schema:
        type: string
      description: comma-separated list of data types, a filter to only include blobs of this given data types
    responses:
      '200':
        description: success
        content:
          application/json:
            schema:
              type: array
              items:
                $ref: "#/components/schemas/DataboxBlobMetadatum"
      '400':
        $ref: 'common_responses.yaml#/components/responses/400BadRequest'
      '401':
        $ref: "common_responses.yaml#/components/responses/401Unauthorized"
      '403':
        $ref: 'common_responses.yaml#/components/responses/403Forbidden'

"blob":
  get:
    summary: download a blob
    description: "Download a blob based on its id"
    tags:
    - Blobs
    parameters:
    - in: path
      name: id
      description: id of the blob
      schema:
        type: string
        format: uuid
      required: true
    responses:
      '200':
        description: Success - return raw data of the blob
        content:
          application/octet-stream:
            schema:
              type: string
              format: binary
      '400':
        $ref: "common_responses.yaml#/components/responses/400BadRequest"
      '401':
        $ref: "common_responses.yaml#/components/responses/401Unauthorized"
      '403':
        $ref: "common_responses.yaml#/components/responses/403Forbidden"
      '404':
        $ref: "common_responses.yaml#/components/responses/404NotFound"

"databox-confirmation-codes":
  post:
    summary: send a confirmation code to your email
    description: "Send a confirmation code to your email, to open a databox"
    tags:
    - Databoxes
    requestBody:
      description: an confirmation code request
      content:
        application/json:
          schema:
            $ref: "#/components/schemas/ConfirmationCode"
    responses:
      '204':
        $ref: "common_responses.yaml#/components/responses/204NoContent"
      '404':
        $ref: "common_responses.yaml#/components/responses/404NotFound"

"databox-access-tokens":
  post:
    summary: create a temporary access token to access to application's databoxes
    description: "Get an access token to be used on all application's databoxes"
    tags:
    - Databoxes
    requestBody:
      description: information to temporary authenticate as application dpo
      content:
        application/json:
          schema:
            $ref: "#/components/schemas/AccessToken"
    responses:
      '201':
        description: status created
        content:
          application/json:
            schema:
              $ref: "#/components/schemas/AccessToken"
      '403':
        $ref: "common_responses.yaml#/components/responses/403Forbidden"
      '404':
        $ref: "common_responses.yaml#/components/responses/404NotFound"

"users-to-delete":
  delete:
    summary: hide all resources linked to the user
    tags:
    - Private
    parameters:
    - in: path
      name: userID
      description: ID of the user
      schema:
        type: string
      required: true
    responses:
      '204':
        $ref: "common_responses.yaml#/components/responses/204NoContent"
      '400':
          $ref: "common_responses.yaml#/components/responses/400BadRequest"
      '403':
          $ref: "common_responses.yaml#/components/responses/403Forbidden"

components:
  schemas:
    Databox:
      type: object
      title: Databox
      description: A databox represents a data place between a user and authorized application.
      required:
        - producer_id
        - owner_id
      properties:
        id:
          description: generated unique UUID
          type: string
          readOnly: true
        owner_id:
          description: ID of the user linked to the databox, owns the databox.
          type: string
        producer_id:
          description: ID of the application responsible for uploading data linked to the owner
          type: string

    DataboxAccessRequest:
      type: object
      title: DataboxAccessRequest
      description: A databox access request represents a way for application to request an temporary access to a databox as a producer.
      required:
        - databox_id
      properties:
        databox_id:
          description: ID of the databox we link to the access request.
          type: string
        token:
          description: access request token, can be used to send a confirmation code to data producing application dpo.
          type: string
          readOnly: true
        producer_name:
          description: data producing application name linked to the access request databox
          type: string
          readOnly: true
        dpo_email:
          description: data producing application dpo email
          type: string
          readOnly: true
        owner_name:
          description: user display name owning the databox linked the to access request
          type: string
          readOnly: true
        owner_email:
          description: user email owning the databox linked the to access request
          type: string
          readOnly: true

    ConfirmationCode:
      type: object
      title: ConfirmationCode
      description: Create a confirmation code resource which is sent to DPO.
      required:
        - access_request_token
      properties:
        access_request_token:
          description: the access request token linked to a databox
          type: string
          writeOnly: true
        id:
          description: unique identifier of the confirmation code
          type: number
          readOnly: true
        databox_id:
          description: databox linked to the confirmation code
          type: string
          readOnly: true
        created_at:
          description: date of creation of the code
          type: string
          format: date-time
          readOnly: true
        consumed_at:
          description: either the code has been used or not
          type: date
          readOnly: true

    AccessToken:
      type: object
      title: AccessToken
      description: gives temporary access to application databoxes
      required:
        - access_request_token
        - confirmation_code
      properties:
        access_request_token:
          description: The access token request of a databox
          type: string
          writeOnly: true
        confirmation_code:
          description: The confirmation code
          type: string
          writeOnly: true
        token:
          description: Generated token to use as Authorization Bearer Token
          type: string
          readOnly: true
        created_at:
          description: date of creation
          type: string
          format: date-time
          readOnly: true
        expires_at:
          description: date of access expiration
          type: string
          format: date-time
          readOnly: true
        subject:
          description: subject linked to the token (a user ID, an application ID...)
          type: string
          readOnly: true
        scope:
          description: space-separated list representing scopes token gives access to
          type: string
          readOnly: true


    DataboxBlobMetadatum:
      type: object
      title: DataboxBlobMetadatum
      description: A databox blob metadata represents information about a blob
      properties:
        id:
          description: ID of the databox blob
          type: string
        databox_id:
          description: ID of the databox link to the blob
          type: string
        transaction_id:
          description: a random value that the client generates on first sending and will re-use to mitigate multi upload in case of network failures - must be unique
          type: string
        data_type:
          description: The type of the data
          type: string
        created_at:
          description: Creation time of the data
          type: string
          format: date-time
        content_length:
          description: The length of the blob
          type: integer
        file_extension:
          description: The file extension - should be format as .*
          type: string
        encryption:
          description: contain some information about the encrypted blob
          type: object
          properties:
            algorithm:
              type: string
              description: must be "com.misakey.nacl-box"
            nonce:
              type: string
              description: (base64-encoded bytes) the nonce (non-secret source of randomness for encryption) that was used during encryption
            ephemeral_producer_pub_key:
              type: string
              description: (base64-encoded bytes) the ephemeral public key created by the producer to encrypt the data
            owner_pub_key:
              type: string
              description: (base64-encoded bytes) the public key of the data owner who owns this blob (to keep track of what was his public key at the time this blob was encrypted, in case it changes)
