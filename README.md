# Introduction
databox-backend, also known as MK Databox is responsible for managing data received from producer, owned by users, retrieved by authorized consumers.
So this project is responsible for Databoxes's business domain:
 - stores and manages databoxes information.
 - stores and manages blobs.

------

## Table of content

* [Introduction](#introduction)
* [Documentation](#documentation)
  * [Protocol](#protocol)
  * [API Documentation](#api-documentation)
* [Dependencies](#dependencies)
  * [Docker](#docker)
* [Folder Architecture](#folder-architecture)
* [Configuration](#configuration)
  * [Environment](#environment)
  * [Configuration File](#configuration-file)
* [Database Migrations](#database-migrations)
  * [Migrate Your Database](#migrate-your-database)
  * [Create Migrations](#create-migrations)
  * [Models generation with SQLBoiler](#models-generation-with-sqlboiler)
* [Deployment](#deployment)

------

## Documentations

### Protocol

** TODO: write protocol functional specification when validated/implemented.**

### API Documentation

An API documentation is available as tools to built it. More information [here](./docs/swagger/README.md).

------

## Dependencies
#### Docker

The project is using [docker](https://www.docker.com/community-edition) to facilitate the run and the deployment.

For both service and migration builds, the default Dockerfile get dependencies over internet.

If you want to use already downloaded dependencies (local), you can use the `Dockerfile.dev` which uses local vendor folder to build binary.

------

## Folder architecture

_Main folders:_
- `src`: databox-backend sources
- `docs`: swagger and postman files
- `config`: application config example and sqlboiler configuration


_Concerning the source code:_

The service aims to follow Clean Architecture principles ([here is a quick introduction](http://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html)), and Layered Architecture.
Here is the list of folders and a description:
- `adaptor`: code linked to frameworks and drivers we use to build our service, mostly for customisation purpose
- `controller`: presenters layers, handling data transformation (usually from JSON to internal model)
- `model`: our model, understandable and used by all our layers
- `repo`: handle retrieval/storage of models (can use a DB, an API...)
- `service`: most of the business logic, interact with repo to play with models/data
- `db`: files for database migration
- `cmd`: possible commands for the service

------

## Configuration

Configuration is done on **2 levels**:
- [some mandatory environment variables](#environment) (used mostly for credentials)
- [a configuration file](#configuration-file) (we use [viper](https://github.com/spf13/viper#what-is-viper) so many possibilities in term of format)

#### Environment

Environment variables are needed for some internal libraries. We also store secret in environment today.

Here is the list of variable to set up to have the databox service working properly:

- `DATABASE_URL`: the [postgres URL](https://jdbc.postgresql.org/documentation/80/connect.html) to connect to the database.
- `ENV`: the environment mode you choose (`production` or `development`):
  - Use of local storage instead of Amazon S3.
  - Print emails on os.Stdout instead of using Amazon SES.
  - Use a different [configuration file](#configuration-file).
- `AWS_ACCESS_KEY` (only if `ENV=production`): [Access key for AWS](https://docs.aws.amazon.com/sdk-for-go/api/aws/credentials/).
- `AWS_SECRET_KEY` (only if `ENV=production`): [Secret key for AWS](https://docs.aws.amazon.com/sdk-for-go/api/aws/credentials/).

#### Configuration File

The service always tries to read the config file from the `/etc` directly.

In development mode, it tries to read `databox-config.dev.{extension}` (extension being one managed by [viper](https://github.com/spf13/viper#what-is-viper)).
This file is automatically added to the built Docker image using `config/databox-config.toml`.

An example a of configuration file (using TOML) is available in `config` folder.
In production mode, it tries to read `databox-config.{extension}` (extension being one managed by [viper](https://github.com/spf13/viper#what-is-viper)).
This file is **NOT** automatically added to the built Docker image. A volume must be mounted to share it.

An example a of configuration file (using TOML) is available in the `config` folder.

------

## Database Migrations

Database Migrations are handled by [Pressly Goose](https://github.com/pressly/goose) which is a fork improving [Liamstask Goose](https://bitbucket.org/liamstask/goose).

The migration tool is encapsulated in a Docker container.

You also need to install SQLBoiler to generate models after database migrations.


#### Migrate Your Database

To migrate the database you just have to launch the `databox_backend` container which run automatically the `databox_migrate` and `databox_db` one.

The default command sent to goose is `up` which apply all missing migrations to your running database.

As an example, if you use docker-compose to run your containers, you can override this command by setting [docker-compose command](https://docs.docker.com/compose/compose-file/compose-file-v2/#command).

The following syntax would perform a `goose down` instead of the default `goose up`.

```yaml
  databox_migrate:
    command: "migrate --goose=down"
```

#### Create Migrations

See [goose usage](https://github.com/pressly/goose#usage) to create migrations.

You must use `goose` locally to create migrations, since the migration file must created on your local storage and not inside a container, it is more practical.
So basically do not use Docker to perform this action :).

A hint of what could look your migration creation command line:

`goose -dir=./src/db/migration/ postgres "postgres://misakey:secret@localhost:5433/databox?sslmode=disable" create add_databox_table`

#### Models generation with SQLBoiler

If you need to alter the SQL database scheme, you'll need to install [sqlboiler](https://github.com/volatiletech/sqlboiler#download) which is the ORM we use.

/!\ You don't need to install it if you just want to migrate the database, this is done automatically using a dedicated Dockerfile in `db` folder.

SQLBoiler is a scheme based ORM which generates models according to SQL scheme in order to optimize queries.

You can regenerate models using the following command, more information [here](https://github.com/volatiletech/sqlboiler#initial-generation):

`sqlboiler psql --wipe --config config/sqlboiler.toml --pkgname model`

SQLBoiler generates by default a `models` folder, so you must move generated files and replace existing one with the new versions.
/!\ We advise to not use `--output=src/model` option since model files unrelated to sqlboiler would be removed.
/!\ Be careful, all existing files will be overriden inside this directory, so be aware of struct tags extension that have been put in place on top of sqlboiler models for example !


## Deployment

:warning: Make sure the corresponding images exist before running the following commands.

Create a `config.yaml` file with a `config:` key and the `config.toml` production content.

When you want to deploy the application for the first time, clone the repo, checkout to the desired tag, then run:

```
helm install --name databox-backend helm/databox-backend -f path/to/config.yaml --set env=production --set ddenv=preprod --set image.tag=latest

```

If you just need to upgrade the application, then run:
```
helm upgrade databox-backend helm/databox-backend -f path/to/config.yaml --set env=production --set ddenv=preprod --set image.tag=latest
```

The command will hang during the migration. Check in another terminal if there is no problem with the migration.

If the migration fails, CTRL+C your helm command, then run `kubectl delete jobs databox-backend-migrate`, then solve the problem and re-run the helm command.
