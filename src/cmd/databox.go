package cmd

import (
	"context"
	"fmt"
	"os"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/Misakey/msk-sdk-go/bubble"
	"gitlab.com/Misakey/msk-sdk-go/config"
	mecho "gitlab.com/Misakey/msk-sdk-go/echo"
	"gitlab.com/Misakey/msk-sdk-go/logger"
	mhttp "gitlab.com/Misakey/msk-sdk-go/rester/http"

	"gitlab.com/Misakey/databox-backend/src/adaptor/sql"
	"gitlab.com/Misakey/databox-backend/src/controller"
	"gitlab.com/Misakey/databox-backend/src/model"
	"gitlab.com/Misakey/databox-backend/src/repo"
	"gitlab.com/Misakey/databox-backend/src/service"
)

var cfgFile string
var goose string
var env = os.Getenv("ENV")

func init() {
	cobra.OnInitialize()
	RootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file")
	RootCmd.PersistentFlags().StringVar(&goose, "goose", "up", "goose command")
	RootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

var RootCmd = &cobra.Command{
	Use:   "databox",
	Short: "Run the application databox service",
	Long:  `This service is responsible for managing data received from producer, owned by users, retrieved by authorized consumers`,
	Run: func(cmd *cobra.Command, args []string) {
		initService()
	},
}

func initService() {
	// init logger
	log.Logger = logger.ZerologLogger()

	// init error needles
	bubble.AddNeedle(bubble.PSQLNeedle{})
	bubble.AddNeedle(bubble.ValidatorNeedle{})
	bubble.AddNeedle(bubble.EchoNeedle{})
	bubble.Lock()

	initDefaultConfig()

	// init echo framework with compressed HTTP responses, custom logger format and custom validator
	e := echo.New()
	e.Use(mecho.NewZerologLogger())
	e.Use(mecho.NewLogger())
	e.Use(mecho.NewCORS())
	e.Use(middleware.Recover())

	e.Validator = mecho.NewValidator()
	e.HTTPErrorHandler = mecho.Error
	e.HideBanner = true

	setRoutes(e)

	// - bind static assets for blobs only if configuration has been set up
	blobsLocation := viper.GetString("server.blobs")
	if len(blobsLocation) > 0 {
		e.Static("/local-blobs", blobsLocation)
	}

	// launch echo server
	e.Logger.Fatal(e.Start(fmt.Sprintf(":%d", viper.GetInt("server.port"))))
}

func setRoutes(e *echo.Echo) {

	// init auth middleware
	jwtMdlw := mecho.NewJWTMidlw(true)

	// init db connection
	dbConn, err := sql.NewPostgreConn(os.Getenv("DATABASE_URL"))
	if err != nil {
		log.Fatal().Err(err)
	}

	// init resters
	applicationRester := mhttp.NewClient(viper.GetString("application.url"), false)
	authRester := mhttp.NewClient(viper.GetString("auth.url"), false)

	// init repositories
	databoxRepo := repo.NewDataboxSQLBoiler(dbConn)
	mailerRepo := getMailer()
	emailTemplateRepo := repo.NewTemplateFileSystem(viper.GetString("mail.templates"))
	applicationRepo := repo.NewApplicationHTTP(applicationRester, viper.GetString("server.token"))
	tokenRepo := repo.NewTokenHTTP(authRester, viper.GetString("server.token"))
	userAccountRepo := repo.NewUserAccountHTTP(authRester, viper.GetString("server.token"))
	blobRepo := repo.NewBlobSQLBoiler(dbConn)

	fileRepo := getBlobRepo()

	emailRenderer, err := service.NewEmailRenderer(
		emailTemplateRepo,
		[]string{"confirm_code_html", "confirm_code_txt"},
		viper.GetString("mail.from"),
	)
	if err != nil {
		log.Fatal().Msgf("email renderer: %v", err)
	}

	databoxManager := service.NewDataboxManager(
		databoxRepo, fileRepo, blobRepo, mailerRepo,
		emailRenderer, applicationRepo, tokenRepo,
		userAccountRepo,
	)

	// init controllers
	databoxCtrl := controller.NewDataboxEcho(databoxManager)
	blobCtrl := controller.NewBlobEcho(databoxManager)

	// Init generic
	genericController := controller.NewGeneric()

	// bind routes
	databoxes := e.Group("databoxes")
	blobs := e.Group("blobs")
	users := e.Group("users")

	databoxes.POST("", databoxCtrl.Create, jwtMdlw)
	databoxes.GET("", databoxCtrl.ListDataboxes, jwtMdlw)
	databoxes.GET("/:id", databoxCtrl.Read, jwtMdlw)
	databoxes.GET("/:id/access-request", databoxCtrl.GetOrCreateAccessRequest, jwtMdlw)
	databoxes.GET("/access-request/:token", databoxCtrl.GetAccessRequest)
	databoxes.POST("/confirmation-code", databoxCtrl.SendConfirmationCode)
	databoxes.POST("/access-token", databoxCtrl.CreateAccessToken)

	blobs.PUT("", blobCtrl.UploadBlob, jwtMdlw)
	blobs.GET("/:id", blobCtrl.DownloadBlob, jwtMdlw)
	blobs.GET("", blobCtrl.ListBlobs, jwtMdlw)

	users.DELETE("/:id", databoxCtrl.Delete, jwtMdlw)

	// Bind generic routes
	generic := e.Group("")
	generic.GET("/version", genericController.Version)

}

func initDefaultConfig() {
	// always find for the /etc folder for the configuration file
	viper.AddConfigPath("/etc/")

	// set defaults
	viper.SetDefault("mail.from", "noreply@misakey.io")
	viper.SetDefault("server.port", 5000)

	// handle missing mandatory fields
	mandatoryFields := []string{
		"mail.templates",
		"application.url",
		"auth.url",
		"server.token",
		"server.cors",
	}

	secretFields := []string{
		"secret.token",
	}

	switch env {
	case "production":
		viper.SetConfigName("databox-config")
		mandatoryFields = append(mandatoryFields, []string{"aws.s3_region", "aws.ses_region", "aws.bucket"}...)
		if os.Getenv("AWS_ACCESS_KEY") == "" {
			log.Warn().Msg("AWS_ACCESS_KEY not set")
		}
		if os.Getenv("AWS_SECRET_KEY") == "" {
			log.Warn().Msg("AWS_SECRET_KEY not set")
		}
	case "development":
		viper.SetConfigName("databox-config.dev")
		mandatoryFields = append(mandatoryFields, "server.blobs")
		log.Info().Msg("{} Development mode is activated. {}")
	default:
		log.Fatal().Msg("unknown ENV value (should be production|development)")
	}

	// try reading in a config
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatal().Err(err).Msgf("could not read configuration")
	}
	config.FatalIfMissing(mandatoryFields)
	config.Print(secretFields)
}

type mailerRepo interface {
	Send(context.Context, *model.Email) error
}

// getMailer defines mailer according to dev env varibable
func getMailer() mailerRepo {
	switch env {
	case "production":
		return repo.NewMailerAmazonSES(viper.GetString("aws.ses_region"))
	case "development":
		return repo.NewLogMailer()
	default:
		log.Fatal().Msg("unknown ENV value (should be production|development)")
	}
	return nil
}

type fileRepo interface {
	UploadBlob(ctx context.Context, bu *model.BlobUpload) (string, error)
	DownloadBlob(bd model.BlobDownload) ([]byte, error)
}

// getblobRepo defines blob according to dev env varibable
func getBlobRepo() fileRepo {
	switch env {
	case "production":
		fr, err := repo.NewSessionAmazonS3(viper.GetString("aws.s3_region"), viper.GetString("aws.bucket"))
		if err != nil {
			log.Fatal().Err(err)
		}
		return fr
	case "development":
		return repo.NewFileSystemBlob(
			viper.GetString("server.blobs"),
			fmt.Sprintf("http://localhost:%d/local-blobs", viper.GetInt("server.port")),
		)
	default:
		log.Fatal().Msg("unknown ENV value (should be production|development)")
	}
	return nil
}
