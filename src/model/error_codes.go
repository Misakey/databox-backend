package model

import (
	"gitlab.com/Misakey/msk-sdk-go/merror"
)

const (
	ForbiddenDataboxID   merror.Code = "forbidden_databox_id"
	InvalidAppID         merror.Code = "invalid_app_id"
	InvalidFormData      merror.Code = "invalid_form_data"
	InvalidPasswordCode  merror.Code = "invalid_password"
	InvalidQueryParam    merror.Code = "invalid_query_param"
	InvalidUserID        merror.Code = "invalid_user_id"
	InvalidFileExtension merror.Code = "invalid_file_extension"
)
