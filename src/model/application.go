package model

// Appplication's backend content and configuration
type Application struct {
	Name     string `json:"name"`
	DPOEmail string `json:"dpo_email"`
}
