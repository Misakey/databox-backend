package model

import (
	"github.com/volatiletech/null"
)

// Filters for Databoxes listing
type DataboxFilters struct {
	Limit      null.Int
	Offset     null.Int
	DataboxIDs []string
	OwnerID    string
	ProducerID string
	WithUsers  *bool
	KeepUserID bool
}
