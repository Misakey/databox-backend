package model

import (
	"io"
)

// BlobUpload represents the blob object request for the upload action
type BlobUpload struct {
	ID      string
	BlobID  string
	Data    io.Reader
	OwnerID string
}

// BlobDownload represents the blob object request for the download action
type BlobDownload struct {
	BlobID  string
	OwnerID string
}
