package model

type Encryption struct {
	Algorithm               string `json:"algorithm" validate:"oneof=com.misakey.nacl-box"`
	Nonce                   string `json:"nonce" validate:"required,base64"`
	EphemeralProducerPubKey string `json:"ephemeral_producer_pub_key" validate:"required,base64"`
	OwnerPubKey             string `json:"owner_pub_key" validate:"required,base64"`
}
