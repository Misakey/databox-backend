package model

type TokenResponse struct {
	Subject       string `json:"subject" validate:"required"`
	TMPAcessToken string `json:"tmp_access_token" validate:"required"`
}

type TokenRequest struct {
	Subject string `json:"subject" validate:"required"`
}
