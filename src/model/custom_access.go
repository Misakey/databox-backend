package model

import "time"

// Request to create an access-token (custom access)
type AccessTokenRequest struct {
	AccessRequestToken string `json:"access_request_token" validate:"required"`
	ConfirmationCode   string `json:"confirmation_code" validate:"required"`
}

// CustomAccess represents a temporary access not necessary linked to a specific user
// inside this service, this custom access are used to:
// - list databoxes linked to an application
// - list blobs meta data linked to a databox
// - upload new blob on a databox
type CustomAccess struct {
	Token     string    `json:"token"`
	CreatedAt time.Time `json:"created_at"`
	ExpiresAt time.Time `json:"expires_at"`
	Subject   string    `json:"subject"`
	Scope     string    `json:"scope"`
}
