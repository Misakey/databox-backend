package model

type UserAccount struct {
	ID          string `json:"id,omitempty"`
	DisplayName string `json:"display_name"`
	Email       string `json:"email"`
}
