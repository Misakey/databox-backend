package repo

import (
	"context"
	"net/url"
	"strings"

	"gitlab.com/Misakey/databox-backend/src/adaptor"
	"gitlab.com/Misakey/databox-backend/src/model"
	"gitlab.com/Misakey/msk-sdk-go/ajwt"
)

// UserAccountHTTP repository retrieve users over HTTP protocol
type UserAccountHTTP struct {
	rester       adaptor.Rester
	serviceToken string
}

// NewUserAccountHTTP is UserAccountHTTP structure constructor
func NewUserAccountHTTP(rester adaptor.Rester, token string) *UserAccountHTTP {
	return &UserAccountHTTP{
		rester:       rester,
		serviceToken: token,
	}
}

func (h *UserAccountHTTP) Get(ctx context.Context, userAccountID string) (*model.UserAccount, error) {
	ua := model.UserAccount{}

	// set service JWT in context
	ctx = ajwt.SetAccessClaimsJWT(ctx, h.serviceToken)

	url := "/users/" + url.PathEscape(userAccountID)

	err := h.rester.Get(ctx, url, nil, &ua)
	return &ua, err
}

func (h *UserAccountHTTP) List(ctx context.Context, userAccountIDs []string) ([]*model.UserAccount, error) {
	users := []*model.UserAccount{}

	// set service JWT in context
	ctx = ajwt.SetAccessClaimsJWT(ctx, h.serviceToken)

	params := url.Values{}
	params.Add("ids", strings.Join(userAccountIDs, ","))

	err := h.rester.Get(ctx, "/users", params, &users)
	return users, err
}
