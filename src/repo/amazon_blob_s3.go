package repo

import (
	"context"
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"gitlab.com/Misakey/msk-sdk-go/logger"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/databox-backend/src/model"
)

type SessionAmazonS3 struct {
	bucket     string
	session    *s3.S3
	uploader   *s3manager.Uploader
	downloader *s3manager.Downloader
}

// NewSessionAmazonS3 init an S3 session
func NewSessionAmazonS3(region, bucket string) (*SessionAmazonS3, error) {
	sess, err := session.NewSession(&aws.Config{
		Region: aws.String(region)},
	)
	if err != nil {
		return nil, fmt.Errorf("could not create aws session (%v)", err)
	}

	// Create S3 service client
	svc := s3.New(sess)

	uploader := s3manager.NewUploader(sess)
	downloader := s3manager.NewDownloaderWithClient(svc)

	s := &SessionAmazonS3{
		bucket:     bucket,
		session:    svc,
		uploader:   uploader,
		downloader: downloader,
	}

	return s, nil
}

// UploadBlob to amazonS3 at {ownerID}/{blobID}
func (s *SessionAmazonS3) UploadBlob(ctx context.Context, blob *model.BlobUpload) (string, error) {
	uo, err := s.uploader.Upload(&s3manager.UploadInput{
		Bucket: aws.String(s.bucket),
		Key:    aws.String(blob.OwnerID + "/" + blob.BlobID),
		Body:   blob.Data,
	})
	if err != nil {
		return "", merror.Internal().Describef("unable to upload %q to %q, %v", blob.BlobID, s.bucket, err)
	}

	logger.FromCtx(ctx).Info().Msgf("successfully uploaded %q to %q\n", blob.BlobID, s.bucket)
	return uo.Location, nil
}

// DownloadBlob from amazonS3 at {ownerID}/{blobID}
func (s *SessionAmazonS3) DownloadBlob(blob model.BlobDownload) ([]byte, error) {
	blobData := aws.NewWriteAtBuffer([]byte{})
	_, err := s.downloader.Download(blobData, &s3.GetObjectInput{
		Bucket: aws.String(s.bucket),
		Key:    aws.String(blob.OwnerID + "/" + blob.BlobID),
	})
	if err != nil {
		return nil, merror.Internal().Describef("unable to download %q to %q, %v", blob.BlobID, s.bucket, err)
	}

	return blobData.Bytes(), nil
}
