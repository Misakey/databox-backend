package repo

import (
	"context"
	"io/ioutil"
	"os"
	"path"

	"github.com/rs/zerolog/log"
	"gitlab.com/Misakey/msk-sdk-go/logger"

	"gitlab.com/Misakey/databox-backend/src/model"
)

// BlobFileSystem
type BlobFileSystem struct {
	location string
	blobURL  string
}

// NewFileSystemBlob init
func NewFileSystemBlob(blobLocation, blobURL string) *BlobFileSystem {
	// Create blob directory
	if _, err := os.Stat(blobLocation); os.IsNotExist(err) {
		err = os.Mkdir(blobLocation, os.ModePerm)
		if err != nil {
			log.Fatal().Err(err).Msg("could not create directory")
		}
	}

	return &BlobFileSystem{
		location: blobLocation,
		blobURL:  blobURL,
	}
}

// UploadBlob : Write a blob in directory and store the path in the db
func (bfs *BlobFileSystem) UploadBlob(ctx context.Context, bu *model.BlobUpload) (string, error) {
	body, err := ioutil.ReadAll(bu.Data)
	if err != nil {
		return "", err
	}

	filePath := path.Join(bfs.location, "/", bu.BlobID)
	f, err := os.Create(filePath)
	if err != nil {
		return "", err
	}

	_, err = f.Write(body)
	if err != nil {
		f.Close()
		return "", err
	}

	logger.FromCtx(ctx).Info().Msg(bu.BlobID + " written successfully at " + filePath)
	err = f.Close()
	if err != nil {
		return "", err
	}

	return bfs.blobURL + "/" + bu.BlobID, nil
}

// DownloadBlob : Download blob and return its raw data
func (bfs *BlobFileSystem) DownloadBlob(bd model.BlobDownload) ([]byte, error) {
	// Constructing the filePath
	filePath := path.Join(bfs.location, "/", bd.BlobID)

	// Reading the file
	data, err := ioutil.ReadFile(filePath)
	if err != nil {
		return nil, err
	}

	return data, nil
}
