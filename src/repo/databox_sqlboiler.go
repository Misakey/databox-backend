package repo

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/lib/pq"
	"github.com/volatiletech/sqlboiler/boil"
	"github.com/volatiletech/sqlboiler/queries/qm"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/databox-backend/src/adaptor/slices"
	"gitlab.com/Misakey/databox-backend/src/model"
)

// UserAccountPSQL manages databox storage using sqlboiler ORM
type DataboxSQLBoiler struct {
	db *sql.DB
}

// DataboxSQLBoiler struct's constructor
func NewDataboxSQLBoiler(db *sql.DB) *DataboxSQLBoiler {
	return &DataboxSQLBoiler{
		db: db,
	}
}

//
// Store a databox into the DB
func (repo *DataboxSQLBoiler) Store(ctx context.Context, databox *model.Databox) error {
	// generate new UUID for new record
	id, err := uuid.NewRandom()
	if err != nil {
		return merror.Internal().Describef("could not generate uuid v4 (%v)", err)
	}

	databox.ID = id.String()

	err = databox.Insert(ctx, repo.db, boil.Infer())
	if err != nil {
		// try to consider error cause as pq error to understand deeper the error
		pqErr, ok := merror.Cause(err).(*pq.Error)
		if !ok { // if not, return error directly
			return err
		}
		if pqErr.Code.Name() == "unique_violation" {
			return merror.Conflict().
				Describe(err.Error()).
				Detail("owner_id", merror.DVConflict).
				Detail("producer_id", merror.DVConflict)
		}
	}
	return err
}

//
// Retrieve databox from DB using ID
func (repo *DataboxSQLBoiler) Retrieve(ctx context.Context, id string) (*model.Databox, error) {
	databox, err := model.FindDatabox(ctx, repo.db, id)
	if err == sql.ErrNoRows {
		return nil, merror.NotFound().
			Describef("databox %s", id).
			Describe(err.Error()).
			Detail("id", merror.DVNotFound)
	}
	return databox, err
}

//
// Delete
func (repo *DataboxSQLBoiler) Delete(ctx context.Context, databox *model.Databox) error {
	databox.ToDelete = true

	rowsAff, err := databox.Update(ctx, repo.db, boil.Infer())
	// if 0 rows has been affected it means record was not found
	if rowsAff == 0 {
		return merror.NotFound().
			Describef("databox %s", databox.ID).
			Detail("id", merror.DVNotFound)
	}
	return err
}

//
// List databoxes from DB
func (repo *DataboxSQLBoiler) List(ctx context.Context, filters model.DataboxFilters) ([]*model.Databox, error) {
	mods := []qm.QueryMod{}

	if filters.Limit.Valid {
		mods = append(mods, qm.Limit(filters.Limit.Int))
	}
	if filters.Offset.Valid {
		mods = append(mods, qm.Offset(filters.Offset.Int))
	}
	if filters.OwnerID != "" {
		mods = append(mods, qm.Where("owner_id = ?", filters.OwnerID))
	}
	if filters.ProducerID != "" {
		mods = append(mods, qm.Where("producer_id = ?", filters.ProducerID))
	}
	if len(filters.DataboxIDs) > 0 {
		query := fmt.Sprintf("id IN (%s)", slices.EscapedStrings(filters.DataboxIDs))
		mods = append(mods, qm.WhereIn(query))
	}
	boxes, err := model.Databoxes(mods...).All(ctx, repo.db)
	if err == nil && boxes == nil {
		return []*model.Databox{}, nil
	}
	return boxes, err
}

// -------------------
// | DATABOX ACCESS  |
// -------------------

//
// Store a databox access request into the DB
func (repo *DataboxSQLBoiler) StoreAccessRequest(ctx context.Context, databoxAR *model.DataboxAccessRequest) error {
	err := databoxAR.Insert(ctx, repo.db, boil.Infer())
	if err != nil {
		// try to consider error cause as pq error to understand deeper the error
		pqErr, ok := merror.Cause(err).(*pq.Error)
		if !ok { // if not, return error directly
			return err
		}
		if pqErr.Code.Name() == "unique_violation" {
			return merror.Conflict().
				Describef("databox access request %s", databoxAR.DataboxID).
				Describe(err.Error()).
				Detail("databox_id", merror.DVConflict)
		}
		if pqErr.Code.Name() == "foreign_key_violation" {
			return merror.Conflict().
				Describef("databox %s", databoxAR.DataboxID).
				Describe(err.Error()).
				Detail("databox_id", merror.DVNotFound)
		}
	}
	return err
}

//
// Retrieve databox from DB using DataboxID - also primary key of the access request
func (repo *DataboxSQLBoiler) RetrieveAccessRequest(ctx context.Context, databoxID string) (*model.DataboxAccessRequest, error) {
	databoxAR, err := model.FindDataboxAccessRequest(ctx, repo.db, databoxID)
	if err == sql.ErrNoRows {
		return nil, merror.NotFound().
			Describef("databox access request %s", databoxID).
			Describe(err.Error()).
			Detail("id", merror.DVNotFound)
	}
	return databoxAR, err
}

// Retrieve databox from DB using Access Request Token
func (repo *DataboxSQLBoiler) RetrieveAccessRequestByToken(ctx context.Context, token string) (*model.DataboxAccessRequest, error) {
	databoxAR, err := model.FindDataboxAccessRequestByToken(ctx, repo.db, token)
	if err == sql.ErrNoRows {
		return nil, merror.NotFound().
			Describe(err.Error()).
			Detail("token", merror.DVNotFound)
	}
	return databoxAR, err
}

// Store a confirmation code into the DB
func (repo *DataboxSQLBoiler) StoreConfirmationCode(ctx context.Context, databoxCC *model.DataboxConfirmationCode) error {
	err := databoxCC.Insert(ctx, repo.db, boil.Infer())
	if err != nil {
		// try to consider error cause as pq error to understand deeper the error
		pqErr, ok := merror.Cause(err).(*pq.Error)
		if !ok { // if not, return error directly
			return err
		}
		if pqErr.Code.Name() == "unique_violation" {
			return merror.Conflict().
				Describef("databox confirmation code %d", databoxCC.ID).
				Describe(err.Error()).
				Detail("id", merror.DVConflict)
		}
		if pqErr.Code.Name() == "foreign_key_violation" {
			return merror.Conflict().
				Describef("databox %s", databoxCC.DataboxID).
				Describe(err.Error()).
				Detail("databox_id", merror.DVNotFound)
		}
	}
	return err
}

// Update confirmation code object from DB using the id of a producer
func (repo *DataboxSQLBoiler) UpdateConfirmationCode(ctx context.Context, dcc *model.DataboxConfirmationCode) error {
	rowsAff, err := dcc.Update(ctx, repo.db, boil.Infer())
	if err != nil {
		return err
	}

	// if 0 rows has been affected it means record was not found
	if rowsAff == 0 {
		return merror.NotFound().
			Describef("databox confirmation code %d", dcc.ID).
			Describe("no rows affected").
			Detail("id", merror.DVNotFound)
	}
	return nil
}

// List confirmation codes with databox id as optional filter
func (repo *DataboxSQLBoiler) ListConfirmationCodes(ctx context.Context, filters model.DataboxCodesFilters) ([]*model.DataboxConfirmationCode, error) {
	mods := []qm.QueryMod{}
	if filters.DataboxID != "" {
		mods = append(mods, qm.Where("databox_id=?", filters.DataboxID))
	}
	if filters.CreatedBefore.Valid {
		mods = append(mods, qm.Where("created_at<?", filters.CreatedBefore.Time))
	}
	if filters.CreatedAfter.Valid {
		mods = append(mods, qm.Where("created_at>?", filters.CreatedAfter.Time))
	}
	if filters.Consumed.Valid {
		if filters.Consumed.Bool {
			mods = append(mods, qm.Where("consumed_at IS NOT NULL"))
		} else {
			mods = append(mods, qm.Where("consumed_at IS NULL"))
		}
	}
	return model.DataboxConfirmationCodes(mods...).All(ctx, repo.db)
}
