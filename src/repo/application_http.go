package repo

import (
	"context"

	"gitlab.com/Misakey/msk-sdk-go/ajwt"

	"gitlab.com/Misakey/databox-backend/src/adaptor"
	"gitlab.com/Misakey/databox-backend/src/model"
)

// ApplicationHTTP
type ApplicationHTTP struct {
	rester adaptor.Rester
	token  string
}

// NewApplicationHTTP is HTTP application-backend structure constructor
func NewApplicationHTTP(rester adaptor.Rester, token string) *ApplicationHTTP {
	return &ApplicationHTTP{
		rester: rester,
		token:  token,
	}
}

// GetApplication retrieves application from application backend using an app ID.
func (h *ApplicationHTTP) GetApplication(ctx context.Context, appID string) (*model.Application, error) {
	app := model.Application{}

	// set service JWT in context
	ctx = ajwt.SetAccessClaimsJWT(ctx, h.token)

	route := "/application-info/" + appID

	err := h.rester.Get(ctx, route, nil, &app)
	return &app, err
}
