package repo

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/lib/pq"
	"github.com/volatiletech/sqlboiler/boil"
	"github.com/volatiletech/sqlboiler/queries/qm"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/databox-backend/src/adaptor/slices"
	"gitlab.com/Misakey/databox-backend/src/model"
)

// BlobSQLBoiler manages blob storage using sqlboiler ORM
type BlobSQLBoiler struct {
	db *sql.DB
}

// BlobSQLBoiler struct's constructor
func NewBlobSQLBoiler(db *sql.DB) *BlobSQLBoiler {
	return &BlobSQLBoiler{
		db: db,
	}
}

//
// Retrieve blob metadata from DB using an ID
func (repo *BlobSQLBoiler) RetrieveBlobMetadata(ctx context.Context, id string) (*model.DataboxBlobMetadatum, error) {
	databoxBlobMetadata, err := model.FindDataboxBlobMetadatum(ctx, repo.db, id)

	if err == sql.ErrNoRows {
		return nil, merror.NotFound().
			Describef("blob metadata %s", id).
			Detail("id", merror.DVNotFound)
	}
	return databoxBlobMetadata, err
}

//
// Delete a databox blob metadata
func (repo *BlobSQLBoiler) DeleteBlobMetadata(ctx context.Context, blobMetadata *model.DataboxBlobMetadatum) error {
	rowsAff, err := blobMetadata.Delete(ctx, repo.db)
	if err != nil {
		return merror.NotFound().
			Describef("blob metadata %s", blobMetadata.ID).
			Detail("id", merror.DVNotFound)
	} else if rowsAff != 1 {
		return merror.Internal().
			Describef("blob metadata %s", blobMetadata.ID).
			Describef("%d rows affected instead of one", rowsAff)
	}
	return nil
}

//
// Store a databox blob metadata into the DB
func (repo *BlobSQLBoiler) StoreBlobMetadata(ctx context.Context, blobMetadata *model.DataboxBlobMetadatum) error {
	// Generate blobID
	blobID, err := uuid.NewRandom()
	if err != nil {
		return merror.Internal().Describef("could not generate uuid v4 (%v)", err)
	}

	blobMetadata.ID = blobID.String()

	err = blobMetadata.Insert(ctx, repo.db, boil.Infer())
	if err != nil {
		// try to consider error cause as pq error to understand deeper the error
		pqErr, ok := merror.Cause(err).(*pq.Error)
		if !ok { // if not, return error directly
			return err
		}
		if pqErr.Code.Name() == "unique_violation" {
			return merror.Conflict().
				Describef("blob metadata %s", blobID).
				Describe(err.Error()).
				Detail("id", merror.DVNotFound)
		}
		if pqErr.Code.Name() == "foreign_key_violation" {
			return merror.Conflict().
				Describef("databox id %s", blobMetadata.DataboxID).
				Describe(err.Error()).
				Detail("databox_id", merror.DVNotFound)
		}
	}
	return err
}

//
// Update a databox blob metadata
func (repo *BlobSQLBoiler) UpdateBlobMetadata(ctx context.Context, blobMetadata *model.DataboxBlobMetadatum) error {
	rowsAff, err := blobMetadata.Update(ctx, repo.db, boil.Infer())
	if err != nil {
		return err
	}

	// if 0 rows has been affected it means record was not found
	if rowsAff == 0 {
		return merror.NotFound().
			Describef("blob metadata %s", blobMetadata.ID).
			Detail("id", merror.DVNotFound)
	}

	return nil
}

func (repo *BlobSQLBoiler) ListBlobMetadata(ctx context.Context, filters model.BlobMetadataFilters) ([]*model.DataboxBlobMetadatum, error) {
	mods := []qm.QueryMod{}
	if filters.Limit.Valid {
		mods = append(mods, qm.Limit(filters.Limit.Int))
	}
	if filters.Offset.Valid {
		mods = append(mods, qm.Offset(filters.Offset.Int))
	}
	if filters.CreatedBefore.Valid {
		mods = append(mods, qm.Where("created_at < ?", filters.CreatedBefore.Time))
	}
	if filters.CreatedAfter.Valid {
		mods = append(mods, qm.Where("created_at > ?", filters.CreatedAfter.Time))
	}
	if len(filters.DataboxIDs) > 0 {
		query := fmt.Sprintf("databox_id IN (%s)", slices.EscapedStrings(filters.DataboxIDs))
		mods = append(mods, qm.WhereIn(query))
	}
	if len(filters.DataTypes) > 0 {
		query := fmt.Sprintf("data_type IN (%s)", slices.EscapedStrings(filters.DataTypes))
		mods = append(mods, qm.WhereIn(query))
	}

	blobs, err := model.DataboxBlobMetadata(mods...).All(ctx, repo.db)
	if err == nil && blobs == nil {
		return []*model.DataboxBlobMetadatum{}, nil
	}
	return blobs, err
}

// DataboxBlobUpload

//
// Store a databox blob upload into the DB
func (repo *BlobSQLBoiler) StoreBlobUpload(ctx context.Context, blobUpload *model.DataboxBlobUpload) error {
	err := blobUpload.Insert(ctx, repo.db, boil.Infer())
	if err != nil {
		// try to consider error cause as pq error to understand deeper the error
		pqErr, ok := merror.Cause(err).(*pq.Error)
		if !ok { // if not, return error directly
			return err
		}
		if pqErr.Code.Name() == "unique_violation" {
			return merror.Conflict().
				Describef("transaction id %s", blobUpload.TransactionID).
				Describe(err.Error()).
				Detail("transaction_id", merror.DVConflict)
		}
		if pqErr.Code.Name() == "foreign_key_violation" {
			return merror.Conflict().
				Describef("blob metadata %s", blobUpload.BlobID).
				Describe(err.Error()).
				Detail("blob_id", merror.DVNotFound)
		}
	}
	return err
}

//
// Retrieve a databox blop upload from the db based on the transaction id
func (repo *BlobSQLBoiler) RetrieveBlobUpload(ctx context.Context, transactionID string) (*model.DataboxBlobUpload, error) {
	blopUpload, err := model.DataboxBlobUploads(qm.Where("transaction_id=?", transactionID)).One(ctx, repo.db)

	if err == sql.ErrNoRows {
		return nil, merror.NotFound().
			Describef("transaction id %s", transactionID).
			Detail("transaction_id", merror.DVNotFound)
	}
	return blopUpload, err
}
