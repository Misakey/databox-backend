package repo

import (
	"context"

	"gitlab.com/Misakey/msk-sdk-go/ajwt"

	"gitlab.com/Misakey/databox-backend/src/adaptor"
	"gitlab.com/Misakey/databox-backend/src/model"
)

// TokenHTTP repository
type TokenHTTP struct {
	rester       adaptor.Rester
	serviceToken string
}

// NewApplicationHTTP is HTTP application-backend structure constructor
func NewTokenHTTP(rester adaptor.Rester, serviceToken string) *TokenHTTP {
	return &TokenHTTP{
		rester:       rester,
		serviceToken: serviceToken,
	}
}

func (h *TokenHTTP) CreateCustomAccess(ctx context.Context, ca *model.CustomAccess) error {
	// set service JWT in context
	ctx = ajwt.SetAccessClaimsJWT(ctx, h.serviceToken)

	route := "/auth/custom-access"
	return h.rester.Post(ctx, route, nil, ca, ca)
}
