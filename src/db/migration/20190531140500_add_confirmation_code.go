package migration

import (
	"database/sql"
	"fmt"

	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(Up20190531140500, Down20190531140500)
}

func Up20190531140500(tx *sql.Tx) error {
	_, err := tx.Exec(`CREATE TABLE databox_confirmation_code(
		id SERIAL PRIMARY KEY,
		databox_id UUID NOT NULL REFERENCES databox ON DELETE CASCADE,
		code VARCHAR (255) NOT NULL,
		created_at timestamptz NOT NULL,
		consumed_at timestamptz NULL
   );`)
	if err != nil {
		return fmt.Errorf("databox_confirmation_code table: %v", err)
	}
	return nil
}

func Down20190531140500(tx *sql.Tx) error {
	_, err := tx.Exec(`DROP TABLE databox_confirmation_code;`)
	if err != nil {
		return fmt.Errorf("databox_confirmation_code table: %v", err)
	}
	return nil
}
