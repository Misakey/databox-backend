package migration

import (
	"database/sql"

	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(Up20190618110516, Down20190618110516)
}

func Up20190618110516(tx *sql.Tx) error {
	_, err := tx.Exec(`ALTER TABLE databox_blob_metadata
		RENAME COLUMN content_lenght to content_length;
	`)
	return err
}

func Down20190618110516(tx *sql.Tx) error {
	_, err := tx.Exec(`ALTER TABLE databox_blob_metadata
		RENAME COLUMN content_length to content_lenght;
	`)
	return err
}
