package migration

import (
	"database/sql"

	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(Up20190703145203, Down20190703145203)
}

func Up20190703145203(tx *sql.Tx) error {
	_, err := tx.Exec(`ALTER TABLE databox
     ADD COLUMN created_at timestamptz NOT NULL,
     ADD COLUMN updated_at timestamptz NOT NULL,
     ADD COLUMN to_delete BOOL NOT NULL DEFAULT false;
	`)
	return err
}

func Down20190703145203(tx *sql.Tx) error {
	_, err := tx.Exec(`ALTER TABLE databox
		DROP COLUMN created_at,
    DROP COLUMN updated_at,
		DROP COLUMN to_delete;
	`)
	return err
}
