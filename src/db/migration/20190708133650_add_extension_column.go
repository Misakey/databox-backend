package migration

import (
	"database/sql"
	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(Up20190708133650, Down20190708133650)
}

func Up20190708133650(tx *sql.Tx) error {
	_, err := tx.Exec(`ALTER TABLE databox_blob_metadata
		ADD COLUMN file_extension VARCHAR(255) NOT NULL;
	`)
	return err
}

func Down20190708133650(tx *sql.Tx) error {
	_, err := tx.Exec(`ALTER TABLE databox_blob_metadata
		DROP COLUMN file_extension;
	`)
	return err
}
