package migration

import (
	"database/sql"
	"fmt"

	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(Up20190605145047, Down20190605145047)
}

func Up20190605145047(tx *sql.Tx) error {
	_, err := tx.Exec(`CREATE TABLE databox_blob_metadata(
		id UUID PRIMARY KEY,
		databox_id UUID NOT NULL REFERENCES databox ON DELETE CASCADE,
		blob_url VARCHAR(255) NOT NULL,
		created_at TIMESTAMP NOT NULL,
		content_lenght BIGINT NOT NULL,
		data_type VARCHAR(255) NOT NULL
   );`)
	if err != nil {
		return fmt.Errorf("databox_blob_metadata table: %v", err)
	}
	return nil
}

func Down20190605145047(tx *sql.Tx) error {
	_, err := tx.Exec(`DROP TABLE databox_blob_metadata;`)
	if err != nil {
		return fmt.Errorf("databox_blob_metadata table: %v", err)
	}
	return nil
}
