package migration

import (
	"fmt"

	"database/sql"
	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(Up20190611152626, Down20190611152626)
}

func Up20190611152626(tx *sql.Tx) error {
	_, err := tx.Exec(`CREATE TABLE databox_blob_upload(
		id SERIAL PRIMARY KEY,
		blob_id UUID UNIQUE NOT NULL REFERENCES databox_blob_metadata ON DELETE CASCADE,
		transaction_id VARCHAR(255) UNIQUE NOT NULL,
		created_at TIMESTAMP NOT NULL
   );`)
   if err != nil {
	   return fmt.Errorf("databox_blob_upload table: %v", err)
   }
   return nil
}

func Down20190611152626(tx *sql.Tx) error {
	_, err := tx.Exec(`DROP TABLE databox_blob_upload;`)
	if err != nil {
		return fmt.Errorf("databox_blob_upload table: %v", err)
	}
	return nil
}
