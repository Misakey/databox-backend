package migration

import (
	"database/sql"
	"fmt"

	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(Up20190528084012, Down20190528084012)
}

func Up20190528084012(tx *sql.Tx) error {
	_, err := tx.Exec(`CREATE TABLE databox(
 		id UUID PRIMARY KEY,
 		owner_id UUID NOT NULL,
		producer_id UUID NOT NULL,
		UNIQUE (owner_id, producer_id)
	);`)
	if err != nil {
		return fmt.Errorf("databox table: %v", err)
	}
	// we can only have one access request per databox
	_, err = tx.Exec(`CREATE TABLE databox_access_request(
		databox_id UUID PRIMARY KEY REFERENCES databox ON DELETE CASCADE,
		token VARCHAR(255) NOT NULL,
		UNIQUE (databox_id)
	);`)
	if err != nil {
		return fmt.Errorf("databox_access_request table: %v", err)
	}
	return nil
}

func Down20190528084012(tx *sql.Tx) error {
	_, err := tx.Exec(`DROP TABLE databox_access_request;`)
	if err != nil {
		return fmt.Errorf("databox_access_request table: %v", err)
	}
	_, err = tx.Exec(`DROP TABLE databox;`)
	if err != nil {
		return fmt.Errorf("databox table: %v", err)
	}
	return nil
}
