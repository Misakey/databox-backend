package migration

import (
	"database/sql"

	"github.com/pressly/goose"
)

func init() {
	goose.AddMigration(Up20190702110340, Down20190702110340)
}

func Up20190702110340(tx *sql.Tx) error {
	_, err := tx.Exec(`ALTER TABLE databox_blob_metadata
	   ADD COLUMN encryption BYTEA NOT NULL;
	`)
	return err
}

func Down20190702110340(tx *sql.Tx) error {
	_, err := tx.Exec(`ALTER TABLE databox_blob_metadata
		DROP COLUMN encryption;
	`)
	return err
}
