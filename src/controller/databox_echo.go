package controller

import (
	"net/http"

	"github.com/google/uuid"
	"github.com/labstack/echo"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/databox-backend/src/adaptor/controller"
	"gitlab.com/Misakey/databox-backend/src/adaptor/param"
	"gitlab.com/Misakey/databox-backend/src/model"
)

type DataboxEcho struct {
	databoxes databoxSrvc
}

// DataboxEcho struct's contructor
func NewDataboxEcho(service databoxSrvc) *DataboxEcho {
	return &DataboxEcho{
		databoxes: service,
	}
}

//
// Handles POST on /databoxes request, create databox entity
func (ctrl *DataboxEcho) Create(ctx echo.Context) error {
	databox := model.Databox{}

	err := ctx.Bind(&databox)
	if err != nil {
		return merror.BadRequest().Describe(err.Error()).From(merror.OriBody)
	}

	err = ctx.Validate(&databox)
	if err != nil {
		return merror.Transform(err).From(merror.OriBody)
	}

	err = ctrl.databoxes.Create(ctx.Request().Context(), &databox)
	if err != nil {
		return merror.Transform(err).Describe("could not create")
	}
	return ctx.JSON(http.StatusCreated, databox)
}

// Read handles GET on /:id request
func (ctrl *DataboxEcho) Read(ctx echo.Context) error {
	id := ctx.Param("id")
	if _, err := uuid.Parse(id); err != nil {
		return merror.BadRequest().From(merror.OriQuery).Detail("id", merror.DVInvalid)
	}

	databox, err := ctrl.databoxes.Read(ctx.Request().Context(), id)
	if err != nil {
		return merror.Transform(err).Describef("could not read databox %s", id)
	}

	return ctx.JSON(http.StatusOK, databox)
}

// ListDataboxes handles GET on /databoxes request
func (ctrl *DataboxEcho) ListDataboxes(ctx echo.Context) error {
	// handle filters parameters
	filters := model.DataboxFilters{}
	var err error

	filters.ProducerID = ctx.QueryParam("producer_id")
	filters.WithUsers, err = param.ToOptionalBool(ctx.QueryParam("with_users"))
	if err != nil {
		return merror.BadRequest().From(merror.OriQuery).Detail("with_users", merror.DVInvalid)
	}
	filters.Limit, err = controller.ParseInt(ctx.QueryParam("limit"))
	if err != nil {
		return merror.BadRequest().From(merror.OriQuery).Detail("limit", merror.DVInvalid).Describe(err.Error())
	}

	filters.Offset, err = controller.ParseInt(ctx.QueryParam("offset"))
	if err != nil {
		return merror.BadRequest().From(merror.OriQuery).Detail("offset", merror.DVInvalid).Describe(err.Error())
	}

	err = ctx.Validate(&filters)
	if err != nil {
		return merror.Transform(err).From(merror.OriQuery)
	}

	// perform listing using filled filters
	boxes, err := ctrl.databoxes.ListDataboxes(ctx.Request().Context(), filters)
	return ctx.JSON(http.StatusOK, boxes)
}

//
// Handles Get on /databoxes/{id}/access-request request, get access request
func (ctrl *DataboxEcho) GetOrCreateAccessRequest(ctx echo.Context) error {
	databoxID := ctx.Param("id")
	if len(databoxID) == 0 {
		return merror.BadRequest().From(merror.OriPath).Detail("id", merror.DVRequired)
	}

	databoxAR, err := ctrl.databoxes.GetOrCreateAccessRequest(ctx.Request().Context(), databoxID)
	if err != nil {
		return merror.Transform(err).Describe("could not get")
	}
	return ctx.JSON(http.StatusCreated, databoxAR)
}

//
// Handles Get on /databoxes/access-request/{token} request, get access request resource
func (ctrl *DataboxEcho) GetAccessRequest(ctx echo.Context) error {
	token := ctx.Param("token")
	if len(token) == 0 {
		return merror.BadRequest().From(merror.OriPath).Detail("token", merror.DVRequired)
	}

	databoxAR, err := ctrl.databoxes.GetAccessRequest(ctx.Request().Context(), token)
	if err != nil {
		return merror.Transform(err).Describe("could not get")
	}
	return ctx.JSON(http.StatusOK, databoxAR)
}

//
// SendConfirmationCode handles POST on /databoxes/confirmation_code request, create and send a confirmation code
func (ctrl *DataboxEcho) SendConfirmationCode(ctx echo.Context) error {
	ccReq := model.ConfirmationCodeRequest{}

	err := ctx.Bind(&ccReq)
	if err != nil {
		return merror.BadRequest().Describe(err.Error()).From(merror.OriBody)
	}

	err = ctx.Validate(&ccReq)
	if err != nil {
		return merror.Transform(err).From(merror.OriBody)
	}

	err = ctrl.databoxes.SendConfirmationCode(ctx.Request().Context(), &ccReq)
	if err != nil {
		return merror.Transform(err).Describe("could not send confirmation code")
	}
	return ctx.NoContent(http.StatusNoContent)
}

// CreateAccessToken handles POST on /databoxes/access-token request
// create an access token
func (ctrl *DataboxEcho) CreateAccessToken(ctx echo.Context) error {
	accessTokenReq := model.AccessTokenRequest{}

	err := ctx.Bind(&accessTokenReq)
	if err != nil {
		return merror.BadRequest().Describe(err.Error()).From(merror.OriBody)
	}

	err = ctx.Validate(&accessTokenReq)
	if err != nil {
		return merror.Transform(err).From(merror.OriBody)
	}

	accessToken, err := ctrl.databoxes.CreateAccessToken(ctx.Request().Context(), &accessTokenReq)
	if err != nil {
		return merror.Transform(err).Describe("could not create access token")
	}

	return ctx.JSON(http.StatusCreated, accessToken)
}

// Delete: handle DELETE on /users/:id request
func (ctrl *DataboxEcho) Delete(ctx echo.Context) error {
	userID := ctx.Param("id")

	err := ctrl.databoxes.Delete(ctx.Request().Context(), userID)
	if err != nil {
		return merror.Transform(err).Describe("could not delete")
	}
	return ctx.NoContent(http.StatusNoContent)
}
