package controller

import (
	"context"
	"io"

	"gitlab.com/Misakey/databox-backend/src/model"
)

type databoxSrvc interface {
	// databoxes
	Create(ctx context.Context, databox *model.Databox) error
	Read(ctx context.Context, id string) (*model.Databox, error)
	Delete(ctx context.Context, userID string) error
	ListDataboxes(ctx context.Context, filters model.DataboxFilters) ([]*model.Databox, error)

	// access requests
	GetOrCreateAccessRequest(ctx context.Context, databoxID string) (*model.DataboxAccessRequest, error)
	GetAccessRequest(ctx context.Context, token string) (*model.DataboxAccessRequest, error)

	// confirmation code & access tokens
	SendConfirmationCode(ctx context.Context, askConfirmCode *model.ConfirmationCodeRequest) error
	CreateAccessToken(ctx context.Context, atReq *model.AccessTokenRequest) (*model.CustomAccess, error)
}

type blobSrvc interface {
	Upload(ctx context.Context, blobMetadata *model.DataboxBlobMetadatum, blobData io.Reader) error
	Download(ctx context.Context, blobID string) ([]byte, error)
	ListBlobs(ctx context.Context, filters model.BlobMetadataFilters) ([]*model.DataboxBlobMetadatum, error)
}
