package controller

import (
	"net/http"
	"regexp"

	"github.com/labstack/echo"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/databox-backend/src/adaptor/controller"
	"gitlab.com/Misakey/databox-backend/src/adaptor/slices"
	"gitlab.com/Misakey/databox-backend/src/model"
)

type BlobEcho struct {
	blobs blobSrvc
}

// DataboxEcho struct's contructor
func NewBlobEcho(service blobSrvc) *BlobEcho {
	return &BlobEcho{
		blobs: service,
	}
}

// UploadBlob Handles PUT on /databoxes/:id/blobs requests
func (ctrl *BlobEcho) UploadBlob(ctx echo.Context) error {
	// Pick blob manually to prevent it of being erase from context during the binding and validating phase
	blob, err := ctx.FormFile("blob")
	if err != nil {
		return merror.BadRequest().From(merror.OriBody).Detail("blob", merror.DVRequired).Describe(err.Error())
	}

	blobMetadata := model.DataboxBlobMetadatum{
		ContentLength: blob.Size,
	}
	err = ctx.Bind(&blobMetadata)
	if err != nil {
		return merror.BadRequest().Describe(err.Error()).From(merror.OriBody)
	}

	matched, err := regexp.Match(`^\.[a-z-1-9-A-Z]+`, []byte(blobMetadata.FileExtension))
	if !matched || err != nil {
		return merror.BadRequest().From(merror.OriBody).
			Describef("invalid file extension: (%v)", err).
			Detail("file_extension", merror.DVInvalid)
	}

	err = ctx.Validate(&blobMetadata)
	if err != nil {
		return merror.Transform(err).From(merror.OriBody)
	}

	blobMetadata.Encryption.Algorithm = ctx.FormValue("encryption[algorithm]")
	blobMetadata.Encryption.Nonce = ctx.FormValue("encryption[nonce]")
	blobMetadata.Encryption.EphemeralProducerPubKey = ctx.FormValue("encryption[ephemeral_producer_pub_key]")
	blobMetadata.Encryption.OwnerPubKey = ctx.FormValue("encryption[owner_pub_key]")

	err = ctx.Validate(&blobMetadata.Encryption)
	if err != nil {
		return merror.Transform(err).From(merror.OriBody)
	}

	blobData, err := blob.Open()
	if err != nil {
		blobData.Close()
		return merror.Internal().Describef("unable to open blob file: (%v)", err)
	}

	err = ctrl.blobs.Upload(ctx.Request().Context(), &blobMetadata, blobData)
	if err != nil {
		return merror.Transform(err).Describe("could not upload")
	}
	err = blobData.Close()
	if err != nil {
		return merror.Internal().Describef("unable to close blob file: (%v)", err)
	}

	return ctx.JSON(http.StatusCreated, blobMetadata)
}

// DownloadBlob Handles GET on /databoxes/blobs/:id requests
func (ctrl *BlobEcho) DownloadBlob(ctx echo.Context) error {
	blobID := ctx.Param("id")
	if len(blobID) == 0 {
		return merror.BadRequest().From(merror.OriPath).Detail("id", merror.DVRequired)
	}

	blobData, err := ctrl.blobs.Download(ctx.Request().Context(), blobID)
	if err != nil {
		return merror.Transform(err).Describe("could not download")
	}

	return ctx.Blob(http.StatusOK, "application/octet-stream", blobData)
}

// ListBlobs handles GET on /blobs request
func (ctrl *BlobEcho) ListBlobs(ctx echo.Context) error {
	// handle filters parameters
	filters := model.BlobMetadataFilters{}
	var err error

	filters.Limit, err = controller.ParseInt(ctx.QueryParam("limit"))
	if err != nil {
		return merror.BadRequest().From(merror.OriPath).Detail("limit", merror.DVInvalid).Describe(err.Error())
	}

	filters.Offset, err = controller.ParseInt(ctx.QueryParam("offset"))
	if err != nil {
		return merror.BadRequest().From(merror.OriPath).Detail("offset", merror.DVInvalid).Describe(err.Error())
	}

	filters.CreatedBefore, err = controller.ParseTime(ctx.QueryParam("created_before"))
	if err != nil {
		return merror.BadRequest().From(merror.OriPath).Detail("created_before", merror.DVInvalid).Describe(err.Error())
	}

	filters.CreatedAfter, err = controller.ParseTime(ctx.QueryParam("created_after"))
	if err != nil {
		return merror.BadRequest().From(merror.OriPath).Detail("created_after", merror.DVInvalid).Describe(err.Error())
	}

	filters.DataTypes = slices.FromSep(ctx.QueryParam("data_types"), ",")
	filters.DataboxIDs = slices.FromSep(ctx.QueryParam("databox_ids"), ",")

	err = ctx.Validate(&filters)
	if err != nil {
		return merror.Transform(err).From(merror.OriPath)
	}

	// perform listing using filled filters
	blobs, err := ctrl.blobs.ListBlobs(ctx.Request().Context(), filters)
	return ctx.JSON(http.StatusOK, blobs)
}
