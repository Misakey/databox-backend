package security

import (
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/databox-backend/src/model"
)

// HashPassword uses argon2 and given string to generate a hash
func HashPassword(password string, cost int) (string, error) {
	encodedHash, err := generateFromPassword(password)
	if err != nil {
		return "", err
	}

	return encodedHash, nil
}

// CheckHashedPassword use argon2 and compare function to check input hashed
// password and source are the same
func CheckHashedPassword(input string, source string) (bool, error) {
	match, err := comparePasswordAndHash(input, source)
	if err != nil {
		return false, err
	}
	if !match {
		return false, merror.New(merror.ForbiddenError, model.InvalidPasswordCode, "wrong password")
	}
	return true, nil
}
