package service

import (
	"time"
)

type DataboxManager struct {
	databoxRepo databoxRepo
	fileRepo    fileRepo
	bRepo       blopRepo

	emailRenderer *emailRenderer

	appRepo   applicationRepo
	userRepo  userAccountRepo
	tokenRepo authTokenRepo
	mailer    mailerRepo

	// confirmation code duration in ms
	otpDuration time.Duration

	customTokenSize uint
}

func NewDataboxManager(
	databoxRepo databoxRepo, fileRepo fileRepo, bRepo blopRepo,
	mailer mailerRepo, emailRenderer *emailRenderer, appRepo applicationRepo,
	tokenRepo authTokenRepo, userRepo userAccountRepo,
) *DataboxManager {
	return &DataboxManager{
		databoxRepo: databoxRepo,
		fileRepo:    fileRepo,
		bRepo:       bRepo,

		emailRenderer:   emailRenderer,
		mailer:          mailer,
		appRepo:         appRepo,
		userRepo:        userRepo,
		tokenRepo:       tokenRepo,
		otpDuration:     300000000000, // in ns == 5 minutes
		customTokenSize: 12,
	}
}
