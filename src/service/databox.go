package service

import (
	"context"

	"gitlab.com/Misakey/msk-sdk-go/ajwt"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/databox-backend/src/model"
)

//
// Create a databox for the given producer_id
// Check producter id does exist in created applications
func (dm *DataboxManager) Create(ctx context.Context, databox *model.Databox) error {
	// check producer_id which is an data producing application exist on its domain
	_, err := dm.appRepo.GetApplication(ctx, databox.ProducerID)
	if err != nil {
		if merror.HasCode(err, merror.NotFoundCode) {
			return merror.Conflict().Detail("producer_id", merror.DVNotFound)
		}
		return err
	}
	// grab current user accesses from context
	// check owner ID is user performing the request
	acc := ajwt.GetAccesses(ctx)
	if acc == nil || acc.IsNotUser(databox.OwnerID) {
		return merror.Forbidden()
	}

	return dm.databoxRepo.Store(ctx, databox)
}

// Read
// check if user is connected as dpo of the producing application
// or as owner of the databox
// return the databox
func (srvc *DataboxManager) Read(ctx context.Context, id string) (*model.Databox, error) {
	acc := ajwt.GetAccesses(ctx)
	if acc == nil {
		return nil, merror.Forbidden()
	}

	// Get the databox first then check access rights
	databox, err := srvc.databoxRepo.Retrieve(ctx, id)
	if err != nil {
		return nil, err
	}

	if acc.IsNotUser(databox.OwnerID) &&
		acc.IsNotDPOOn(databox.ProducerID) {
		return nil, merror.Forbidden()
	}

	// if dpo, we hide ownerID
	if !acc.IsNotDPOOn(databox.ProducerID) {
		databox.OwnerID = ""
	}

	return databox, nil
}

// Delete
// set to_delete on true for all the databoxes owned by the connected user
func (dm *DataboxManager) Delete(ctx context.Context, userID string) error {
	filters := model.DataboxFilters{}

	acc := ajwt.GetAccesses(ctx)
	if acc == nil {
		return merror.Forbidden()
	}

	// service can delete any userID's databoxes
	// otherwise, only the connected user can delete his databox
	if acc.IsNotUser(userID) && acc.IsNotAnyService() {
		return merror.Forbidden()
	}

	filters.OwnerID = userID

	databoxes, err := dm.databoxRepo.List(ctx, filters)
	if err != nil {
		return err
	}

	for _, databox := range databoxes {
		if err := dm.databoxRepo.Delete(ctx, databox); err != nil {
			return err
		}
	}

	return err
}

// List
func (dm *DataboxManager) ListDataboxes(ctx context.Context, filters model.DataboxFilters) ([]*model.Databox, error) {
	acc := ajwt.GetAccesses(ctx)
	if acc == nil {
		return nil, merror.Forbidden()
	}

	if err := dm.setDataboxFilters(acc, &filters); err != nil {
		return nil, err
	}

	databoxes, err := dm.databoxRepo.List(ctx, filters)
	if err != nil {
		return nil, err
	}

	// add users if WithUsers filter has been asked
	if filters.WithUsers != nil && *filters.WithUsers {
		err := dm.addUserToDataboxes(ctx, databoxes, filters)
		if err != nil {
			return nil, err
		}
	}

	// if the call has not been done by the owner, we hide info about owner id
	if !filters.KeepUserID {
		for _, d := range databoxes {
			d.OwnerID = ""
			if d.User != nil {
				d.User.ID = ""
			}
		}
	}

	return databoxes, err
}

// setDataboxFilters ProducerID or OwnerID depending on user scopes & roles
func (dm *DataboxManager) setDataboxFilters(acc *ajwt.AccessClaims, filters *model.DataboxFilters) error {
	dpoAppID := acc.GetDPOAppID()
	if acc.IsAnyApp() {
		filters.ProducerID = acc.Subject
	} else if dpoAppID != nil {
		filters.ProducerID = *dpoAppID
	} else if acc.IsAnyUser() {
		filters.OwnerID = acc.Subject
		filters.KeepUserID = true
	} else {
		return merror.Forbidden()
	}
	return nil
}

// List users based on OwnerIDs of given databoxes and bind them inside considering databox struct
func (dm *DataboxManager) addUserToDataboxes(
	ctx context.Context,
	databoxes []*model.Databox,
	filters model.DataboxFilters,
) error {
	// user IDs list to get owner
	userIDs := []string{}

	// build list of user ids to list
	for _, databox := range databoxes {
		userIDs = append(userIDs, databox.OwnerID)
	}

	users, err := dm.userRepo.List(ctx, userIDs)
	if err != nil {
		return err
	}

	// build a map of uuid:users to get efficiently users later
	userMap := make(map[string]*model.UserAccount, len(users))
	for _, user := range users {
		userMap[user.ID] = user
	}

	// bind users to their databoxes
	for i := range databoxes {
		user, ok := userMap[databoxes[i].OwnerID]
		if ok {
			databoxes[i].User = user
		}
	}
	return nil
}

// checkAskedDataboxes are allowed to be seen by the user
func (dm *DataboxManager) checkAskedDataboxes(filters model.BlobMetadataFilters, databoxes []*model.Databox) error {
	if len(filters.DataboxIDs) == 0 {
		return merror.Forbidden().Describe("asked databoxes are empty")
	}

	// compute list of databox IDs retrieved from DB.
	databoxIDs := []string{}
	for _, databox := range databoxes {
		databoxIDs = append(databoxIDs, databox.ID)
	}

	// we return a forbidden if ONE of the asked IDs is not part of the retrieved IDs from DB.
Loop:
	for _, askedID := range filters.DataboxIDs {
		for _, allowedID := range databoxIDs {
			if askedID == allowedID {
				continue Loop
			}
		}
		return merror.Forbidden().Describef("cannot access %s", askedID)
	}
	return nil
}
