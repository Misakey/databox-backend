package service

import (
	"context"
	"html/template"

	"gitlab.com/Misakey/databox-backend/src/model"
)

type databoxRepo interface {
	Store(ctx context.Context, databox *model.Databox) error
	Retrieve(ctx context.Context, id string) (*model.Databox, error)
	Delete(ctx context.Context, databox *model.Databox) error
	List(ctx context.Context, filters model.DataboxFilters) ([]*model.Databox, error)

	RetrieveAccessRequest(ctx context.Context, databoxID string) (*model.DataboxAccessRequest, error)
	StoreAccessRequest(ctx context.Context, ar *model.DataboxAccessRequest) error
	RetrieveAccessRequestByToken(ctx context.Context, token string) (*model.DataboxAccessRequest, error)

	StoreConfirmationCode(ctx context.Context, databoxCC *model.DataboxConfirmationCode) error
	UpdateConfirmationCode(ctx context.Context, dcc *model.DataboxConfirmationCode) error
	ListConfirmationCodes(ctx context.Context, filters model.DataboxCodesFilters) ([]*model.DataboxConfirmationCode, error)
}

type templateRepo interface {
	Load(name string) error
	Get(name string) (*template.Template, error)
}

type mailerRepo interface {
	Send(ctx context.Context, email *model.Email) error
}

type applicationRepo interface {
	GetApplication(ctx context.Context, appID string) (*model.Application, error)
}

type authTokenRepo interface {
	CreateCustomAccess(ctx context.Context, ca *model.CustomAccess) error
}

type userAccountRepo interface {
	Get(ctx context.Context, userAccountID string) (*model.UserAccount, error)
	List(ctx context.Context, userAccountIDs []string) ([]*model.UserAccount, error)
}

type fileRepo interface {
	UploadBlob(ctx context.Context, bu *model.BlobUpload) (string, error)
	DownloadBlob(bd model.BlobDownload) ([]byte, error)
}

type blopRepo interface {
	DeleteBlobMetadata(ctx context.Context, blobMetadata *model.DataboxBlobMetadatum) error
	RetrieveBlobMetadata(ctx context.Context, id string) (*model.DataboxBlobMetadatum, error)
	StoreBlobMetadata(ctx context.Context, blobMetadata *model.DataboxBlobMetadatum) error
	UpdateBlobMetadata(ctx context.Context, blobMetadata *model.DataboxBlobMetadatum) error
	ListBlobMetadata(ctx context.Context, filters model.BlobMetadataFilters) ([]*model.DataboxBlobMetadatum, error)

	RetrieveBlobUpload(ctx context.Context, transactionID string) (*model.DataboxBlobUpload, error)
	StoreBlobUpload(ctx context.Context, blobUpload *model.DataboxBlobUpload) error
}
