package service

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/Misakey/msk-sdk-go/ajwt"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/databox-backend/src/model"
)

func TestSetDataboxFilters(t *testing.T) {
	tests := map[string]struct {
		acc             *ajwt.AccessClaims
		filters         model.DataboxFilters
		expectedErr     error
		expectedFilters model.DataboxFilters
	}{
		"a call without jwt cannot list blobs": {
			acc:         &ajwt.AccessClaims{},
			expectedErr: merror.Forbidden(),
		},
		"a call with scope service cannot list blobs": {
			acc:         &ajwt.AccessClaims{Scope: "service"},
			expectedErr: merror.Forbidden(),
		},
		"a call with scope user can list their blobs": {
			acc: &ajwt.AccessClaims{Subject: "1", Scope: "user"},
			filters: model.DataboxFilters{
				DataboxIDs: []string{"databox_uuid_1"},
				OwnerID:    "",
				ProducerID: "",
				KeepUserID: true,
			},
			expectedErr: nil,
			expectedFilters: model.DataboxFilters{
				DataboxIDs: []string{"databox_uuid_1"},
				OwnerID:    "1",
				ProducerID: "",
				KeepUserID: true,
			},
		},
		"a call with scope user cannot list other users blobs by forcing OwnerID to Subject": {
			acc: &ajwt.AccessClaims{Subject: "1", Scope: "user"},
			filters: model.DataboxFilters{
				DataboxIDs: []string{"databox_uuid_1"},
				OwnerID:    "2",
				ProducerID: "",
				KeepUserID: true,
			},
			expectedErr: nil,
			expectedFilters: model.DataboxFilters{
				DataboxIDs: []string{"databox_uuid_1"},
				OwnerID:    "1",
				ProducerID: "",
				KeepUserID: true,
			},
		},
		"a call with role dpo can list blobs where they have rights on": {
			acc: &ajwt.AccessClaims{Subject: "1", Scope: "user rol.dpo.00000000-1111-2222-3333-444444444444"},
			filters: model.DataboxFilters{
				DataboxIDs: []string{"databox_uuid_1"},
				OwnerID:    "",
				ProducerID: "",
			},
			expectedErr: nil,
			expectedFilters: model.DataboxFilters{
				DataboxIDs: []string{"databox_uuid_1"},
				ProducerID: "00000000-1111-2222-3333-444444444444",
				OwnerID:    "",
			},
		},
		"a call with application scope can list their blobs": {
			acc: &ajwt.AccessClaims{Subject: "app_1", Scope: "application"},
			filters: model.DataboxFilters{
				DataboxIDs: []string{"databox_uuid_1"},
				OwnerID:    "",
				ProducerID: "",
			},
			expectedErr: nil,
			expectedFilters: model.DataboxFilters{
				DataboxIDs: []string{"databox_uuid_1"},
				ProducerID: "app_1",
				OwnerID:    "",
			},
		},
	}

	for description, test := range tests {

		t.Run(description, func(t *testing.T) {
			// init service with mocked sub-layers
			service := DataboxManager{}
			// call function to test
			err := service.setDataboxFilters(test.acc, &test.filters)

			// check assertions
			assert.Equal(t, test.expectedErr, err)
			assert.Equal(t, test.expectedFilters, test.filters)
		})

	}
}

func TestCheckAskDataboxes(t *testing.T) {
	tests := map[string]struct {
		filters     model.BlobMetadataFilters
		databoxes   []*model.Databox
		expectedErr error
	}{
		"retrieved databoxes with the same as the requested ones is allowed": {
			filters:     model.BlobMetadataFilters{DataboxIDs: []string{"databox_1", "databox_2"}},
			databoxes:   []*model.Databox{&model.Databox{ID: "databox_2"}, &model.Databox{ID: "databox_1"}},
			expectedErr: nil,
		},
		"one of the retrieved databoxes being different from the requested ones is not allowed": {
			filters:     model.BlobMetadataFilters{DataboxIDs: []string{"databox_2", "databox_3"}},
			databoxes:   []*model.Databox{&model.Databox{ID: "databox_1"}, &model.Databox{ID: "databox_2"}},
			expectedErr: merror.Forbidden().Describef("cannot access databox_3"),
		},
		"all retrieved databoxes being different from the requested ones is not allowed": {
			filters:     model.BlobMetadataFilters{DataboxIDs: []string{"databox_4", "databox_5"}},
			databoxes:   []*model.Databox{&model.Databox{ID: "databox_1"}, &model.Databox{ID: "databox_2"}},
			expectedErr: merror.Forbidden().Describef("cannot access databox_4"),
		},
		"one of the databoxes retrieved is missing from the requested ones is not allowed": {
			filters:     model.BlobMetadataFilters{DataboxIDs: []string{"databox_4", "databox_5", "databox_6"}},
			databoxes:   []*model.Databox{&model.Databox{ID: "databox_4"}, &model.Databox{ID: "databox_5"}},
			expectedErr: merror.Forbidden().Describef("cannot access databox_6"),
		},
		"requested databoxes is an empty slice should return an error": {
			filters:     model.BlobMetadataFilters{DataboxIDs: []string{}},
			databoxes:   []*model.Databox{&model.Databox{ID: "databox_4"}, &model.Databox{ID: "databox_5"}},
			expectedErr: merror.Forbidden().Describef("asked databoxes are empty"),
		},
	}

	for description, test := range tests {

		t.Run(description, func(t *testing.T) {
			// init service with mocked sub-layers
			service := DataboxManager{}
			// call function to test
			err := service.checkAskedDataboxes(test.filters, test.databoxes)

			// check assertions
			assert.Equal(t, test.expectedErr, err)
		})

	}
}
