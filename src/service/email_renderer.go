package service

import (
	"bytes"
	"context"
	"fmt"

	"github.com/pkg/errors"

	"gitlab.com/Misakey/databox-backend/src/model"
)

type emailRenderer struct {
	templateRepo templateRepo

	// from emails address
	mailFrom string
}

// NewEmailRenderer is mailRenderer's constructor
// It takes:
// - a templateRepo that abstract the way we get email NewTemplateFileSystem
// - a template to preload list
// - parameters about how to build emails...
func NewEmailRenderer(
	templateRepo templateRepo,
	toLoad []string,
	mailFrom string,
) (*emailRenderer, error) {
	renderer := &emailRenderer{
		templateRepo: templateRepo,
		mailFrom:     mailFrom,
	}
	err := renderer.load(toLoad...)
	return renderer, err
}

// load a template inside repository
func (m *emailRenderer) load(names ...string) error {
	var errs error
	for _, name := range names {
		err := m.templateRepo.Load(name)
		if err != nil {
			errs = errors.Wrap(errs, err.Error())
		}
	}
	if errs != nil {
		return fmt.Errorf("could not load some templates: (%s)", errs.Error())
	}
	return nil
}

// NewEmail return an new email structure filled with all necessary information to be sent.
// data must be a map[string]interface{} corresponding to template indicated by the templateName string.
func (m *emailRenderer) NewEmail(
	ctx context.Context,
	to string,
	subject string,
	templateName string,
	data map[string]interface{},
) (*model.Email, error) {
	email := &model.Email{
		To:      to,
		From:    m.mailFrom,
		Subject: subject,
	}
	// render html
	htmlBody, err := m.render(ctx, fmt.Sprintf("%s_html", templateName), data)
	if err != nil {
		return nil, err
	}
	// render text
	textBody, err := m.render(ctx, fmt.Sprintf("%s_txt", templateName), data)
	if err != nil {
		return nil, err
	}

	email.HTMLBody = string(htmlBody)
	email.TextBody = string(textBody)
	return email, nil
}

// render retrieves template from repo, executes it with given data then returns its final content
func (m *emailRenderer) render(ctx context.Context, templateName string, data map[string]interface{}) (output []byte, err error) {
	buf := &bytes.Buffer{}

	tmpl, err := m.templateRepo.Get(templateName)
	if err != nil {
		return nil, fmt.Errorf("could not get template: %v", err)
	}

	err = tmpl.Execute(buf, data)
	if err != nil {
		return nil, fmt.Errorf("could not render %s template (%v)", templateName, err)
	}

	return buf.Bytes(), nil
}
