package service

import (
	"context"
	"time"

	"github.com/volatiletech/null"
	"gitlab.com/Misakey/msk-sdk-go/logger"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/databox-backend/src/model"
	"gitlab.com/Misakey/databox-backend/src/service/security"
)

//
// SendConfirmationCode to the owner of the box
func (dm *DataboxManager) SendConfirmationCode(ctx context.Context, ccReq *model.ConfirmationCodeRequest) error {
	databoxCC := &model.DataboxConfirmationCode{}

	// Get Access Request
	accessReq, err := dm.databoxRepo.RetrieveAccessRequestByToken(ctx, ccReq.AccessRequestToken)
	if err != nil {
		return err
	}

	// Get databox
	databox, err := dm.databoxRepo.Retrieve(ctx, accessReq.DataboxID)
	if err != nil {
		return err
	}

	// Get Email DPO from application-backend with producing ID
	app, err := dm.appRepo.GetApplication(ctx, databox.ProducerID)
	if err != nil {
		return err
	}

	// Generate Confirmation Code
	code, err := security.GenerateOTP()
	if err != nil {
		return merror.Internal().Describef("could not generate confirmation code (%v)", err)
	}

	// Hashed Confirmation Code
	hashedCode, err := security.HashOTP(code)
	if err != nil {
		return merror.Internal().Describef("could not hashed confirmation code (%v)", err)
	}

	databoxCC.Code = hashedCode
	databoxCC.DataboxID = databox.ID

	// Save confirmation code in db
	if err = dm.databoxRepo.StoreConfirmationCode(ctx, databoxCC); err != nil {
		return err
	}

	// notify asynchronously user about confirmation code
	// Send the unencrypted one
	go func() {
		if err := dm.notifyConfirm(ctx, app.DPOEmail, code, app.Name); err != nil {
			logger.FromCtx(ctx).Warn().Err(err).Msgf("could not notify confirm to %s", app.DPOEmail)
		}
	}()

	return nil
}

func (dm *DataboxManager) notifyConfirm(ctx context.Context, to string, code string, appName string) error {
	data := map[string]interface{}{
		"to":       to,
		"app_name": appName,
		"code":     code,
	}
	email, err := dm.emailRenderer.NewEmail(ctx, to, "Votre code de validation - Misakey", "confirm_code", data)
	if err != nil {
		return err
	}
	return dm.mailer.Send(ctx, email)
}

//
// Based on a confirmation code, generate a custom access for a producer ID and return it
func (dm *DataboxManager) CreateAccessToken(ctx context.Context, atReq *model.AccessTokenRequest) (*model.CustomAccess, error) {
	// Get Access Request
	accessReq, err := dm.databoxRepo.RetrieveAccessRequestByToken(ctx, atReq.AccessRequestToken)
	if err != nil {
		return nil, err
	}

	// compute time code are still valid
	validTimeLimit := time.Now().UTC().Add(-dm.otpDuration)

	// list valid codes linked to the databox
	codes, err := dm.databoxRepo.ListConfirmationCodes(ctx, model.DataboxCodesFilters{
		DataboxID:    accessReq.DataboxID,
		CreatedAfter: null.TimeFrom(validTimeLimit),
		Consumed:     null.BoolFrom(false),
	})
	if err != nil {
		return nil, err
	}
	if len(codes) == 0 {
		return nil, merror.BadRequest().
			Describef("no valid confirmation codes found for %s", accessReq.DataboxID).
			Detail("confirmation_code", merror.DVInvalid)
	}

	var matchCode *model.DataboxConfirmationCode
	// check one of the retrieved codes exist
	// break directly the loop and set matchCode if found
	// error from hash function should be considered as 500, it should never occur
	// to happen would mean our data has been wrongly hashed or hacked which is critical
	for _, code := range codes {
		check, err := security.CheckHashedOTP(atReq.ConfirmationCode, code.Code)
		if err != nil {
			return nil, merror.Internal().
				Describef("code hash check failed, corrupted data (%d): (%s)", code.ID, err.Error()).
				Detail("confirmation_code", merror.DVInvalid)
		}
		if check {
			matchCode = code
			break
		}
	}

	if matchCode == nil {
		return nil, merror.BadRequest().
			Describef("invalid confirmation code %s", atReq.ConfirmationCode).
			Detail("confirmation_code", merror.DVInvalid)
	}

	// get databox to get data producing application info to build custom access token
	databox, err := dm.databoxRepo.Retrieve(ctx, accessReq.DataboxID)
	if err != nil {
		return nil, err
	}

	// Set up wished custom access and create it on auth server
	// Subject is application ID since we don't have any registered user.
	ca := &model.CustomAccess{
		Subject: databox.ProducerID,
		Scope:   applicationScope,
	}
	err = dm.tokenRepo.CreateCustomAccess(ctx, ca)
	if err != nil {
		return nil, err
	}

	// consume the code
	matchCode.ConsumedAt = null.TimeFrom(time.Now().UTC())
	err = dm.databoxRepo.UpdateConfirmationCode(ctx, matchCode)
	if err != nil {
		return nil, err
	}

	return ca, nil
}
