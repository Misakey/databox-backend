package service

import (
	"context"
	"encoding/json"
	"io"

	"gitlab.com/Misakey/msk-sdk-go/ajwt"
	"gitlab.com/Misakey/msk-sdk-go/logger"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/databox-backend/src/model"
)

// UploadBlob in file local storage or in a cloud provider
func (dm *DataboxManager) Upload(ctx context.Context, blobMetadata *model.DataboxBlobMetadatum, blobContent io.Reader) error {
	acc := ajwt.GetAccesses(ctx)
	if acc == nil {
		return merror.Forbidden()
	}

	databox, err := dm.databoxRepo.Retrieve(ctx, blobMetadata.DataboxID)
	if err != nil {
		return err
	}

	if acc.IsNotUser(databox.OwnerID) &&
		acc.IsNotApp(databox.ProducerID) &&
		acc.IsNotDPOOn(databox.ProducerID) {
		return merror.Forbidden()
	}

	// Construct Blob Upload
	blopUpload := model.DataboxBlobUpload{
		TransactionID: blobMetadata.TransactionID,
	}

	// Check if the transaction has already been done or in progress
	existingBlopUpload, err := dm.bRepo.RetrieveBlobUpload(ctx, blobMetadata.TransactionID)
	// If the blob upload already exist, we return directly information about the already created blob
	if existingBlopUpload != nil {
		// Retrieve the blob metadata
		existingBlopMetadata, err := dm.bRepo.RetrieveBlobMetadata(ctx, existingBlopUpload.BlobID)
		// we raise an error only if different from "not found" because we want to create the
		// resource if it has been not found
		if err != nil {
			return err
		}

		// Check if data type and transaction id are the same
		if existingBlopMetadata.DataType == blobMetadata.DataType {
			// Copy the value of the  existingblobmetadata into the blobmetadata pointer
			*blobMetadata = *existingBlopMetadata
			blobMetadata.TransactionID = existingBlopUpload.TransactionID
			return nil
		}
	}

	// we raise an error only if different from "not found" because we want to create the
	// resource if it has been not found
	if !merror.HasCode(err, merror.NotFoundCode) {
		return err
	}

	// Encode Encryption before insert
	blobMetadata.EncodedEncryption, err = json.Marshal(blobMetadata.Encryption)
	if err != nil {
		return err
	}

	// Store BlobMetadata
	err = dm.bRepo.StoreBlobMetadata(ctx, blobMetadata)
	if err != nil {
		return err
	}

	blopUpload.BlobID = blobMetadata.ID

	// Add Blob Upload in DB
	err = dm.bRepo.StoreBlobUpload(ctx, &blopUpload)
	if err != nil {
		return err
	}

	// Struct use to upload the data blob to storage
	blob := &model.BlobUpload{
		BlobID:  blobMetadata.ID,
		Data:    blobContent,
		OwnerID: databox.OwnerID,
	}

	// start the upload
	url, err := dm.fileRepo.UploadBlob(ctx, blob)
	if err != nil {
		// Delete BlobMetadata from DB
		errorDel := dm.bRepo.DeleteBlobMetadata(ctx, blobMetadata)
		if errorDel != nil {
			return errorDel
		}
		return err
	}

	blobMetadata.BlobURL = url

	// Insert/Update
	err = dm.bRepo.UpdateBlobMetadata(ctx, blobMetadata)
	if err != nil {
		return err
	}

	return nil
}

// DownloadBlob from local storage or cloud provider
func (dm *DataboxManager) Download(ctx context.Context, blobID string) ([]byte, error) {
	// grab current user accesses from context
	acc := ajwt.GetAccesses(ctx)
	if acc == nil || acc.IsNotAnyUser() {
		return nil, merror.Forbidden()
	}

	blobMetadata, err := dm.bRepo.RetrieveBlobMetadata(ctx, blobID)
	if err != nil {
		return nil, merror.New(merror.ForbiddenError, merror.ForbiddenCode, "invalid blob")
	}

	// Get databox to use its owner information
	databox, err := dm.databoxRepo.Retrieve(ctx, blobMetadata.DataboxID)
	if err != nil {
		return nil, err
	}

	// Check if owner of the file to retrieve
	if databox.OwnerID != acc.Subject {
		return nil, merror.New(merror.ForbiddenError, merror.ForbiddenCode, "invalid user")
	}

	blobDownload := model.BlobDownload{
		BlobID:  blobID,
		OwnerID: databox.OwnerID,
	}

	blobData, err := dm.fileRepo.DownloadBlob(blobDownload)
	if err != nil {
		return nil, err
	}

	return blobData, nil
}

//
func (dm *DataboxManager) ListBlobs(ctx context.Context, filters model.BlobMetadataFilters) ([]*model.DataboxBlobMetadatum, error) {
	acc := ajwt.GetAccesses(ctx)
	if acc == nil {
		return nil, merror.Forbidden()
	}

	// we first retrieve databox user is allowed to see, & we force filters this list
	databoxFilters := model.DataboxFilters{
		DataboxIDs: filters.DataboxIDs,
	}

	err := dm.setDataboxFilters(acc, &databoxFilters)
	if err != nil {
		return nil, err
	}

	databoxes, err := dm.databoxRepo.List(ctx, databoxFilters)
	if err != nil {
		return nil, err
	}

	err = dm.checkAskedDataboxes(filters, databoxes)
	if err != nil {
		return nil, err
	}

	// return directly databoxes if nothing found
	if len(databoxes) == 0 {
		return []*model.DataboxBlobMetadatum{}, nil
	}

	// force filters with retrieve databoxes IDs considering accesses and ownership
	boxes, err := dm.bRepo.ListBlobMetadata(ctx, filters)
	if err != nil {
		return nil, err
	}

	// for each blob found, set the Encryption interface
	for i := range boxes {
		err := json.Unmarshal(boxes[i].EncodedEncryption, &boxes[i].Encryption)
		if err != nil {
			logger.FromCtx(ctx).Warn().Err(err).Msg("could not Unmarshal Encryption metadata")
		}
	}

	return boxes, err
}
