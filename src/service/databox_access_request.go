package service

import (
	"context"

	"gitlab.com/Misakey/msk-sdk-go/ajwt"
	"gitlab.com/Misakey/msk-sdk-go/merror"

	"gitlab.com/Misakey/databox-backend/src/model"
	"gitlab.com/Misakey/databox-backend/src/service/security"
)

//
// Retrieve a databox access request using a token
func (dm *DataboxManager) GetAccessRequest(ctx context.Context, arToken string) (*model.DataboxAccessRequest, error) {
	// try to retrieve the access request
	databoxAR, err := dm.databoxRepo.RetrieveAccessRequestByToken(ctx, arToken)
	// if access request has not been found, we auto generate it.
	if err != nil {
		return nil, err
	}

	// retrieve databox
	databox, err := dm.databoxRepo.Retrieve(ctx, databoxAR.DataboxID)
	if err != nil {
		return nil, err
	}

	return dm.enhancedAR(ctx, databoxAR, databox)
}

func (dm *DataboxManager) enhancedAR(
	ctx context.Context,
	databoxAR *model.DataboxAccessRequest,
	databox *model.Databox,
) (*model.DataboxAccessRequest, error) {
	// set producing data application name
	app, err := dm.appRepo.GetApplication(ctx, databox.ProducerID)
	if err != nil {
		return nil, merror.Transform(err).Describe("cannot retrieve producer")
	}
	databoxAR.ProducerName = app.Name
	databoxAR.DPOEmail = app.DPOEmail

	// set user account owning data name
	ua, err := dm.userRepo.Get(ctx, databox.OwnerID)
	if err != nil {
		return nil, merror.Transform(err).Describe("cannot retrieve owner")
	}
	databoxAR.OwnerID = databox.OwnerID
	databoxAR.OwnerName = ua.DisplayName
	databoxAR.OwnerEmail = ua.Email
	return databoxAR, nil
}

//
// Retrieve a databox access request for the given databox_id
// Generate it if does not exist yet
func (dm *DataboxManager) GetOrCreateAccessRequest(ctx context.Context, databoxID string) (*model.DataboxAccessRequest, error) {
	// grab current user accesses from context
	acc := ajwt.GetAccesses(ctx)
	if acc == nil {
		return nil, merror.Forbidden()
	}

	// retrieve databox and check current user is the owner
	databox, err := dm.databoxRepo.Retrieve(ctx, databoxID)
	if err != nil {
		return nil, err
	}
	if acc.IsNotUser(databox.OwnerID) {
		return nil, merror.Forbidden().Describe("invalid user").
			Detail("owner_id", merror.DVForbidden)
	}

	// try to retrieve the access request
	databoxAR, err := dm.databoxRepo.RetrieveAccessRequest(ctx, databoxID)
	if err != nil {
		// if access request error means it has not been found, we skip it to auto-create it
		if !merror.HasCode(err, merror.NotFoundCode) {
			return nil, err
		}
		databoxAR, err = dm.createAccessRequest(ctx, databoxID)
		if err != nil {
			return nil, err
		}
	}
	return dm.enhancedAR(ctx, databoxAR, databox)
}

func (dm *DataboxManager) createAccessRequest(ctx context.Context, databoxID string) (*model.DataboxAccessRequest, error) {
	// generate a token
	token, err := security.AccessRequestToken(dm.customTokenSize)
	if err != nil {
		return nil, merror.Internal().Describe("failed to generate random string")
	}

	databoxAR := &model.DataboxAccessRequest{
		DataboxID: databoxID,
		Token:     token,
	}

	err = dm.databoxRepo.StoreAccessRequest(ctx, databoxAR)
	return databoxAR, err
}
