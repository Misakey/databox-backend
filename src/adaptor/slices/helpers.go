package slices

import "strings"

// EscapedStrings take a slice of strings and return it as a string
// with quote around elems of the list, separated by commas
// The purpose of this helper is to facilitate WHERE IN query building
func EscapedStrings(strSlice []string) string {
	var escaped []string
	for _, elem := range strSlice {
		if string(elem[0]) != "'" {
			escaped = append(escaped, "'"+elem+"'")
		}
	}
	return strings.Join(escaped, ",")
}

// FromSep use strings.Split to split a {sep}-separated list string into a slice
// It handles the fact strings.Split return an slice of size 1 containing empty string if the sepStr is empty
// We return then, an empty slice instead of this default strings.Split behavior
func FromSep(sepStr string, sep string) []string {
	if len(sepStr) == 0 {
		return []string{}
	}
	return strings.Split(sepStr, sep)
}
