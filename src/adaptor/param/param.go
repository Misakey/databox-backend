package param

import "strconv"

// ToOptionalBool take a string and return a pointer on a boolean
func ToOptionalBool(param string) (*bool, error) {
	if len(param) == 0 {
		return nil, nil
	}
	b, err := strconv.ParseBool(param)
	if err != nil {
		return nil, err
	}
	return &b, nil
}
