package controller

import (
	"strconv"
	"time"

	"github.com/volatiletech/null"
)

func ParseTime(param string) (null.Time, error) {
	if param != "" {
		timestamp, err := time.Parse(time.RFC3339, param)
		if err != nil {
			return null.Time{}, err
		}
		return null.TimeFrom(timestamp), nil
	}
	return null.Time{}, nil
}

func ParseInt(param string) (null.Int, error) {
	if param != "" {
		integer, err := strconv.ParseInt(param, 10, 32)
		if err != nil {
			return null.Int{}, err
		}
		return null.IntFrom(int(integer)), nil
	}
	return null.Int{}, nil
}
